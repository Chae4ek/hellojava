package ru.omsu.finances;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FinanceReportProcessorTest {
  @Test
  public void findAllAt1() {
    FinanceReport in = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2),
        new Payment("Филиппов Алексей", 21, 12, 2000, 22));
    FinanceReport fr = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2));

    assertEquals(fr, FinanceReportProcessor.findAllAt('Н', in));
  }

  @Test
  public void findAllAt2() {
    FinanceReport in = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2),
        new Payment("Филиппов Алексей", 21, 12, 2000, 22));
    FinanceReport fr = new FinanceReport("a b c", 20, 11, 2001);

    assertEquals(fr, FinanceReportProcessor.findAllAt('Х', in));
  }

  @Test
  public void findAllLessThen() {
    FinanceReport in = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2),
        new Payment("Филиппов Алексей", 21, 12, 2000, 22));
    FinanceReport fr = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2));

    assertEquals(fr, FinanceReportProcessor.findAllLessThen(22, in));
  }

  @Test
  public void getAllSum() {
    FinanceReport report = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2),
        new Payment("Филиппов Алексей", 21, 12, 2000, 22), new Payment("some челик", 21, 12, 2000, 4));

    assertEquals(26, FinanceReportProcessor.getAllSum("21.12.2000", report));
  }

  @Test
  public void getMonthsWithoutPayments1() {
    FinanceReport report = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 2, 2000, 2),
        new Payment("Филиппов Алексей", 21, 11, 1999, 22), new Payment("some челик", 21, 12, 2000, 4));
    String[] answer = new String[] { "Январь", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь",
        "Ноябрь" };

    assertArrayEquals(answer, FinanceReportProcessor.getMonthsWithoutPayments(2000, report));
  }

  @Test
  public void getMonthsWithoutPayments2() {
    FinanceReport report = new FinanceReport("a b c", 20, 11, 2001, new Payment("Номинас Алексей", 1, 1, 2000, 2),
        new Payment("Филиппов Алексей", 21, 2, 2000, 22), new Payment("some челик", 21, 3, 2000, 4),
        new Payment("some челик", 21, 4, 2000, 4), new Payment("some челик", 21, 5, 2000, 4),
        new Payment("some челик", 21, 6, 2000, 4), new Payment("some челик", 21, 7, 2000, 4),
        new Payment("some челик", 21, 8, 2000, 4), new Payment("some челик", 21, 9, 2000, 4),
        new Payment("some челик", 21, 10, 2000, 4), new Payment("some челик", 21, 11, 2000, 4),
        new Payment("some челик", 21, 12, 2000, 4));
    String[] answer = new String[] { "" };

    assertArrayEquals(answer, FinanceReportProcessor.getMonthsWithoutPayments(2000, report));
  }
}

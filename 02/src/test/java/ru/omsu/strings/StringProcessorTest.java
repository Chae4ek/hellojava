package ru.omsu.strings;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StringProcessorTest {
  @Test
  public void clone1() throws Exception {
    assertEquals("asdasdasd", StringProcessor.clone("asd", 3));
  }

  @Test
  public void clone2() throws Exception {
    assertEquals("", StringProcessor.clone("asd", 0));
  }

  @Test(expected = Exception.class)
  public void clone3() throws Exception {
    StringProcessor.clone("asd", -1);
  }

  @Test
  public void countOccur1() throws Exception {
    assertEquals(3, StringProcessor.countOccur("asdasdasd", "asd"));
    // assertEquals(3, StringProcessor.countOccur("aaaaa", "aaa"));
  }

  @Test
  public void countOccur2() throws Exception {
    assertEquals(3, StringProcessor.countOccur("aaa", "a"));
  }

  @Test
  public void countOccur3() throws Exception {
    assertEquals(0, StringProcessor.countOccur("ddd", "a"));
  }

  @Test(expected = Exception.class)
  public void countOccur4() throws Exception {
    assertEquals(3, StringProcessor.countOccur("asd", null));
  }

  @Test
  public void replaceNumbers1() {
    assertEquals("одинfdsдвадватри", StringProcessor.replaceNumbers("1fds223"));
  }

  @Test
  public void replaceNumbers2() {
    assertEquals("asd", StringProcessor.replaceNumbers("asd"));
  }

  @Test
  public void delete1() {
    StringBuilder sb = new StringBuilder();
    sb.append("12345");

    StringProcessor.delEvenChars(sb);

    assertEquals("135", sb.toString());
  }

  @Test
  public void delete2() {
    StringBuilder sb = new StringBuilder();
    sb.append("12");

    StringProcessor.delEvenChars(sb);

    assertEquals("1", sb.toString());
  }

  @Test
  public void delete3() {
    StringBuilder sb = new StringBuilder();
    sb.append("1");

    StringProcessor.delEvenChars(sb);

    assertEquals("1", sb.toString());
  }

  @Test
  public void reverseWords1() {
    assertEquals("  dd  cc bbb aaa", StringProcessor.reverseWords("  aaa  bbb cc dd"));
  }

  @Test
  public void reverseWords2() {
    assertEquals("  dd  cc bbb aaa  ", StringProcessor.reverseWords("  aaa  bbb cc dd  "));
  }

  @Test
  public void reverseWords3() {
    assertEquals("dd  cc bbb aaa", StringProcessor.reverseWords("aaa  bbb cc dd"));
  }

  @Test
  public void reverseWords4() {
    assertEquals(" aa ", StringProcessor.reverseWords(" aa "));
    assertEquals("", StringProcessor.reverseWords(""));
    assertEquals(" aa aa ", StringProcessor.reverseWords(" aa aa "));
    assertEquals(" aa   aab  ", StringProcessor.reverseWords(" aab   aa  "));
    assertEquals("   ", StringProcessor.reverseWords("   "));
  }

  @Test
  public void interpNumbers1() {
    assertEquals("Васе 16 лет", StringProcessor.interpNumbers("Васе 0x00000010 лет"));
  }

  @Test
  public void interpNumbers2() {
    assertEquals("16Васе  лет", StringProcessor.interpNumbers("0x00000010Васе  лет"));
  }

  @Test
  public void interpNumbers3() {
    assertEquals("Васе  лет16", StringProcessor.interpNumbers("Васе  лет0x00000010"));
  }

  @Test
  public void interpNumbers4() {
    assertEquals("Васе  лет", StringProcessor.interpNumbers("Васе  лет"));
    assertEquals("Васе 10 лет", StringProcessor.interpNumbers("Васе 0xa лет"));
    assertEquals("Васе 0x лет 0xg", StringProcessor.interpNumbers("Васе 0x лет 0xg"));
    assertEquals("Васе 10 11 12 лет", StringProcessor.interpNumbers("Васе 0x0a 0xB 0x00C лет"));

  }
}

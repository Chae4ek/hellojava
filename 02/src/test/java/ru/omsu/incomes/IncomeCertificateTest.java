package ru.omsu.incomes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IncomeCertificateTest {
  @Test
  public void getMonthsIncomes1() {
    long[] s = new long[] { 1, 2, 3 };
    IncomeCertificate c = new IncomeCertificate(0, "", "", s);

    s[0] = 0;

    assertEquals(1, c.getMonthsIncomes()[0]);
  }

  @Test
  public void getMonthsIncomes2() {
    IncomeCertificate c = new IncomeCertificate(0, "", "", 1, 2, 3);

    c.getMonthsIncomes()[0] = 0;

    assertEquals(1, c.getMonthsIncomes()[0]);
  }
}

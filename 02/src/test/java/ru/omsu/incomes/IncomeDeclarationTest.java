package ru.omsu.incomes;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IncomeDeclarationTest {
  @Test
  public void getMonthsIncomes() {
    IncomeDeclaration d = new IncomeDeclaration(0, "",
        new IncomeCertificate(0, "", "", 120000, 230000, 2400000, 2500000, 0, 0, 0, 0, 0, 0, 0, 0),
        new IncomeCertificate(0, "", "", 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

    assertArrayEquals(new long[] { 120000, 235000, 2400000, 2500000, 0, 0, 0, 0, 0, 0, 0, 0 }, d.getMonthsIncomes());
  }

  @Test
  public void getSumMonthsIncomes() {
    IncomeDeclaration d = new IncomeDeclaration(0, "",
        new IncomeCertificate(0, "", "", 120000, 230000, 2400000, 2500000, 0, 0, 0, 0, 0, 0, 0, 0),
        new IncomeCertificate(0, "", "", 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

    assertArrayEquals(new long[] { 120000, 355000, 2755000, 5255000, 5255000, 5255000, 5255000, 5255000, 5255000,
        5255000, 5255000, 5255000 }, d.getSumMonthsIncomes());
  }

  @Test
  public void getIncomeTaxes() {
    IncomeDeclaration d = new IncomeDeclaration(0, "",
        new IncomeCertificate(0, "", "", 120000, 230000, 2400000, 2500000, 0, 0, 0, 0, 0, 0, 0, 0),
        new IncomeCertificate(0, "", "", 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

    assertArrayEquals(
        new long[] { 0, 0, 46150, 371150, 371150, 371150, 371150, 371150, 371150, 371150, 371150, 371150 },
        d.getIncomeTaxes());
  }

  @Test
  public void getSumIncomeTaxes() {
    IncomeDeclaration d = new IncomeDeclaration(0, "",
        new IncomeCertificate(0, "", "", 120000, 230000, 2400000, 2500000, 0, 0, 0, 0, 0, 0, 0, 0),
        new IncomeCertificate(0, "", "", 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

    assertEquals(371150, d.getSumIncomeTaxes());
  }
}

package ru.omsu.incomes;

public class IncomeCertificate {
  private int year;
  private String fullName;
  private String company;
  private long[] monthsIncomes;

  public IncomeCertificate(int year, String fullName, String company, long... monthsIncomes) {
    this.year = year;
    this.fullName = new String(fullName);
    this.company = new String(company);
    this.monthsIncomes = new long[12];
    for (int i = 0; i < monthsIncomes.length; ++i) {
      this.monthsIncomes[i] = monthsIncomes[i];
    }
  }

  public int getYear() {
    return year;
  }

  public String getFullName() {
    return fullName;
  }

  public String getCompany() {
    return company;
  }

  public long[] getMonthsIncomes() {
    long[] result = new long[12];
    for (int i = 0; i < 12; ++i) {
      result[i] = monthsIncomes[i];
    }
    return result;
  }
}

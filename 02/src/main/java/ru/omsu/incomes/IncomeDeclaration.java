package ru.omsu.incomes;

public class IncomeDeclaration {
  private int year;
  private String fullName;
  private long[] sumMonthsIncomes;
  private long sumIncomeTaxes = -1;

  public IncomeDeclaration(int year, String fullName, IncomeCertificate... incomeCertificates) {
    this.year = year;
    this.fullName = new String(fullName);
    this.sumMonthsIncomes = new long[12];
    for (int i = 0; i < 12; ++i) {
      if (i > 0) {
        sumMonthsIncomes[i] += sumMonthsIncomes[i - 1];
      }
      for (IncomeCertificate certificate : incomeCertificates) {
        sumMonthsIncomes[i] += certificate.getMonthsIncomes()[i];
      }
    }
  }

  public int getYear() {
    return year;
  }

  public String getFullName() {
    return fullName;
  }

  public long[] getMonthsIncomes() {
    long[] result = new long[12];
    result[0] = sumMonthsIncomes[0];
    for (int i = 1; i < 12; ++i) {
      result[i] = sumMonthsIncomes[i] - sumMonthsIncomes[i - 1];
    }
    return result;
  }

  public long[] getSumMonthsIncomes() {
    long[] result = new long[12];
    for (int i = 0; i < 12; ++i) {
      result[i] = sumMonthsIncomes[i];
    }
    return result;
  }

  public long[] getIncomeTaxes() {
    long[] result = new long[12];
    sumIncomeTaxes = 0;
    for (int i = 0; i < 12; ++i) {
      if (sumMonthsIncomes[i] > 240000_00) {
        result[i] = (long) ((sumMonthsIncomes[i] - 240000_00) * 0.2);
        result[i] += 28080_00;
      } else if (sumMonthsIncomes[i] > 24000_00) {
        result[i] = (long) ((sumMonthsIncomes[i] - 24000_00) * 0.13);
      }

    }
    return result;
  }

  public long getSumIncomeTaxes() {
    return getIncomeTaxes()[11];
  }
}

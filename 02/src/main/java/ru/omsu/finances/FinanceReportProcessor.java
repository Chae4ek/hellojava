package ru.omsu.finances;

public class FinanceReportProcessor {
  /**
   * Получение платежей всех людей, чья фамилия начинается на указанный символ
   * (символ — входной параметр).
   */
  public static FinanceReport findAllAt(char startSecondName, FinanceReport in) {
    int length = in.getCountPayments();

    Payment[] payments = new Payment[in.getCountPayments()];

    int countPayments = 0;
    for (int i = 0; i < length; ++i) {
      Payment payment = in.getPayment(i);

      if (payment.getFullName().charAt(0) == startSecondName) {
        payments[countPayments++] = payment;
      }
    }

    Payment[] realPayments = new Payment[countPayments];

    int realCount = 0;
    for (Payment p : payments) {
      if (p != null) {
        realPayments[realCount++] = p;
      }
    }

    return new FinanceReport(in.getFullName(), in.getDay(), in.getMonth(), in.getYear(), realPayments);
  }

  /**
   * Получение всех платежей, размер которых меньше заданной величины.
   */
  public static FinanceReport findAllLessThen(int value, FinanceReport in) {
    int length = in.getCountPayments();

    Payment[] payments = new Payment[in.getCountPayments()];

    int countPayments = 0;
    for (int i = 0; i < length; ++i) {
      Payment payment = in.getPayment(i);

      if (payment.getPaymentAmount() < value) {
        payments[countPayments++] = payment;
      }
    }

    Payment[] realPayments = new Payment[countPayments];

    int realCount = 0;
    for (Payment p : payments) {
      if (p != null) {
        realPayments[realCount++] = p;
      }
    }

    return new FinanceReport(in.getFullName(), in.getDay(), in.getMonth(), in.getYear(), realPayments);
  }

  /**
   * Суммарный платеж на заданную дату в формате dd.mm.yy
   */
  public static int getAllSum(String date, FinanceReport report) {
    int sum = 0;

    String[] dateSplit = date.split("[.]");
    int day = Integer.parseInt(dateSplit[0]);
    int month = Integer.parseInt(dateSplit[1]);
    int year = Integer.parseInt(dateSplit[2]);

    for (int i = 0; i < report.getCountPayments(); ++i) {
      Payment p = report.getPayment(i);
      if (p.getDay() == day && p.getMonth() == month && p.getYear() == year) {
        sum += report.getPayment(i).getPaymentAmount();
      }
    }

    return sum;
  }

  /**
   * Получение списка названий месяцев, в которых не было ни одного платежа в
   * течении указанного года
   */
  public static String[] getMonthsWithoutPayments(int year, FinanceReport report) {
    int[] months = new int[12];

    for (int i = 0; i < report.getCountPayments(); ++i) {
      Payment p = report.getPayment(i);
      if (p.getYear() == year) {
        months[p.getMonth() - 1] = 1;
      }
    }

    StringBuilder sb = new StringBuilder();

    if (months[0] == 0)
      sb.append("Январь/");
    if (months[1] == 0)
      sb.append("Февраль/");
    if (months[2] == 0)
      sb.append("Март/");
    if (months[3] == 0)
      sb.append("Апрель/");
    if (months[4] == 0)
      sb.append("Май/");
    if (months[5] == 0)
      sb.append("Июнь/");
    if (months[6] == 0)
      sb.append("Июль/");
    if (months[7] == 0)
      sb.append("Август/");
    if (months[8] == 0)
      sb.append("Сентябрь/");
    if (months[9] == 0)
      sb.append("Октябрь/");
    if (months[10] == 0)
      sb.append("Ноябрь/");
    if (months[11] == 0)
      sb.append("Декабрь/");

    return sb.toString().split("/");
  }
}

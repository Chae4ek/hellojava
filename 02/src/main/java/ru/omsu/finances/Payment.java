package ru.omsu.finances;

public class Payment {
  private String fullName;
  private int day;
  private int month;
  private int year;
  private int paymentAmount;

  /**
   * @param day           1 <= day <= 31
   * @param month         1 <= month <= 12
   * @param year          1 <= year <= 2077
   * @param paymentAmount в копейках
   */
  public Payment(String fullName, int day, int month, int year, int paymentAmount) {
    this.fullName = new String(fullName);
    this.day = day;
    this.month = month;
    this.year = year;
    this.paymentAmount = paymentAmount;
  }

  public Payment(Payment p) {
    this(p.fullName, p.day, p.month, p.year, p.paymentAmount);
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getPaymentAmount() {
    return paymentAmount;
  }

  public void setPaymentAmount(int paymentAmount) {
    this.paymentAmount = paymentAmount;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    Payment payment = (Payment) obj;

    return this.fullName.equals(payment.fullName) && this.day == payment.day && this.month == payment.month
        && this.year == payment.year && this.paymentAmount == payment.paymentAmount;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
    result = prime * result + day;
    result = prime * result + month;
    result = prime * result + year;
    result = prime * result + paymentAmount;
    return result;
  }

  @Override
  public String toString() {
    return String.format("Плательщик: %s, дата: %d.%d.%d, сумма: %d руб. %d коп.", fullName, day, month, year,
        paymentAmount / 100, paymentAmount % 100);
  }
}

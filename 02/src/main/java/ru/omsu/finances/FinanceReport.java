package ru.omsu.finances;

public class FinanceReport {
  private Payment[] payments;
  private String fullNameReporter;
  private int day;
  private int month;
  private int year;

  public FinanceReport(String fullNameReporter, int day, int month, int year, Payment... payments) {
    this.fullNameReporter = new String(fullNameReporter);
    this.day = day;
    this.month = month;
    this.year = year;

    this.payments = new Payment[payments.length];
    int i = 0;
    for (Payment p : payments) {
      this.payments[i] = new Payment(p);
      ++i;
    }
  }

  public FinanceReport(FinanceReport fr) {
    this(fr.fullNameReporter, fr.day, fr.month, fr.year, fr.payments);
  }

  public int getCountPayments() {
    return payments.length;
  }

  public void setPayment(int index, Payment payment) {
    payments[index] = payment;
  }

  public Payment getPayment(int index) {
    return payments[index];
  }

  public String getFullName() {
    return fullNameReporter;
  }

  public void setFullName(String fullNameReporter) {
    this.fullNameReporter = fullNameReporter;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(String.format("[Автор: %s, дата: %d.%d.%d, Платежи: [", fullNameReporter, day, month, year));

    for (Payment p : payments) {
      sb.append("\n\t").append(p.toString()).append(',');
    }

    sb.deleteCharAt(sb.length() - 1);
    sb.append("]]");

    return sb.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    FinanceReport fr = (FinanceReport) obj;

    boolean isEqual = this.fullNameReporter.equals(fr.fullNameReporter) && this.day == fr.day && this.month == fr.month
        && this.year == fr.year;

    if (isEqual) {
      for (int i = 0; i < payments.length; ++i) {
        if (!this.payments[i].equals(fr.payments[i])) {
          return false;
        }
      }
    }

    return isEqual;
  }
}

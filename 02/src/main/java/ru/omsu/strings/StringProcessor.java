package ru.omsu.strings;

import java.util.regex.Pattern;
import java.math.BigInteger;
import java.util.regex.Matcher;

public abstract class StringProcessor {
  /**
   * На входе строка s и целое число N. Выход — строка, состоящая из N копий
   * строки s, записанных подряд. При N=0 результат — пустая строка. При N < 0
   * выбрасывается исключение.
   */
  public static String clone(String s, int N) {
    if (N < 0) {
      throw new IllegalArgumentException("out of bounds");
    }

    StringBuilder sb = new StringBuilder();

    for (; N > 0; --N) {
      sb.append(s);
    }

    return sb.toString();
  }

  /**
   * На входе две строки. Результат — количество вхождений второй строки в первую.
   * Если вторая строка пустая или null, выбросить исключение.
   */
  public static int countOccur(String str, String subStr) throws Exception {
    if (subStr == null || subStr.equals("")) {
      throw new Exception("subStr is null");
    }
    return (str.length() - str.replace(subStr, "").length()) / subStr.length();
  }

  /**
   * Постройте по строке новую строку, которая получена из исходной заменой
   * каждого символа '1' на подстроку "один”, символа ‘2’ на подстроку “два” и
   * символа ‘3’ на подстроку “три”.
   */
  public static String replaceNumbers(String s) {
    StringBuilder sb = new StringBuilder();

    if (s != null) {
      for (char i : s.toCharArray()) {
        switch (i) {
          case '1':
            sb.append("один");
            break;
          case '2':
            sb.append("два");
            break;
          case '3':
            sb.append("три");
            break;
          default:
            sb.append(i);
            break;
        }
      }
    }

    return sb.toString();
  }

  /**
   * В строке типа StringBuilder удалите каждый второй по счету символ. Должна
   * быть модифицирована входная строка, а не порождена новая.
   */
  public static void delEvenChars(StringBuilder sb) {
    // if (sb != null) {
    for (int i = 1; i < sb.length(); ++i) {
      sb.deleteCharAt(i);
    }
    // }
  }

  /**
   * Дана строка, состоящая из слов. Словом считается группа символов, отделенная
   * от других слов пробелами. Пробельных символов между словами может быть
   * несколько. Также пробелы могут быть в начале строки и в ее конце. Надо
   * построить новую строку, в которой все слова будут идти в обратном порядке, а
   * пробелы останутся на местах. Т.е. строка вида “aaabbbccdd” должна
   * превратиться в “ddccbbbaaa”.
   */
  public static String reverseWords(String s) {
    if (s == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder();

    String[] strs = s.split(" ++");
    int i = strs.length;

    for (int j = 0; j < s.length();) {
      if (s.charAt(j) == ' ') {
        sb.append(' ');
        ++j;
      } else {
        while (j < s.length() && s.charAt(j) != ' ') {
          ++j;
        }
        sb.append(strs[--i]);
      }
    }

    return sb.toString();
  }

  /**
   * Строка содержит подстроки вида 0xNNNNNNNN, где N—шестнадцатеричная цифра.
   * Постройте новую строку, в которой все шестнадцатеричные числа будут заменены
   * на десятичные эквиваленты (например, строка «Васе 0x00000010 лет» должна
   * превратиться в строку «Васе 16 лет»)
   */
  public static String interpNumbers(String s) {
    StringBuilder sb = new StringBuilder();

    Matcher matcher = Pattern.compile("0x[0-9A-Fa-f]{1,}").matcher(s);
    int end = 0;
    while (matcher.find()) {
      int start = matcher.start();

      sb.append(s.substring(end, start));
      end = matcher.end();
      sb.append(new BigInteger(s.substring(start + 2, end), 16));
    }
    sb.append(s.substring(end, s.length()));

    return sb.toString();
  }
}

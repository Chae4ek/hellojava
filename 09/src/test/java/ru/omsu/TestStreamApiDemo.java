package ru.omsu;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

public class TestStreamApiDemo {

  @Test
  public void testRemoveAllNull() {
    List<Integer> list1 = new ArrayList<>();
    list1.add(1);
    list1.add(2);
    list1.add(3);
    list1.add(null);
    list1.add(4);
    list1.add(null);
    list1.add(5);
    list1.add(6);
    list1.add(null);
    List<Integer> list2 = new ArrayList<>();
    list2.add(null);

    var ans1 = StreamApiDemo.removeAllNull.apply(list1);
    var ans2 = StreamApiDemo.removeAllNull.apply(list2);

    Assert.assertEquals(List.of(1, 2, 3, 4, 5, 6), ans1);
    Assert.assertEquals(new ArrayList<Integer>(), ans2);
  }

  @Test
  public void testPositiveCount() {
    Set<Integer> set = Set.of(1, -1, 2, 3, 4, -5, 0);

    var ans = StreamApiDemo.positiveCount.applyAsLong(set);

    Assert.assertEquals(4, ans);
  }

  @Test
  public void testLast3Elements() {
    List<Integer> list1 = List.of(1, 2, 3, 4, 5, 6);
    List<Integer> list2 = List.of(1, 2, 3);
    List<Integer> list3 = List.of(1, 2);

    var ans1 = StreamApiDemo.last3Elements.apply(list1);
    var ans2 = StreamApiDemo.last3Elements.apply(list2);
    var ans3 = StreamApiDemo.last3Elements.apply(list3);

    Assert.assertEquals(List.of(4, 5, 6), ans1);
    Assert.assertEquals(List.of(1, 2, 3), ans2);
    Assert.assertEquals(List.of(1, 2), ans3);
  }

  @Test
  public void testFirstEven() {
    List<Integer> list1 = new ArrayList<>();
    list1.add(1);
    list1.add(-1);
    list1.add(3);
    list1.add(null);
    list1.add(4);
    list1.add(5);
    list1.add(6);
    List<Integer> list2 = new ArrayList<>();
    list2.add(1);
    list2.add(3);
    list2.add(null);
    list2.add(5);

    var ans1 = StreamApiDemo.firstEven.apply(list1);
    var ans2 = StreamApiDemo.firstEven.apply(list2);

    Assert.assertEquals(Integer.valueOf(4), ans1);
    Assert.assertEquals(null, ans2);
  }

  @Test
  public void testSquaredListWithoutEquals() {
    int[] arr = {0, 1, -1, 3, 2, -2};

    var ans = StreamApiDemo.squaredListWithoutEquals.apply(arr);
    ans.sort((a, b) -> a - b);

    Assert.assertEquals(List.of(0, 1, 4, 9), ans);
  }

  @Test
  public void testSortedNonEmpty() {
    List<String> list1 = new ArrayList<>();
    list1.add("1");
    list1.add("2");
    list1.add("");
    list1.add(null);
    list1.add("3");
    List<String> list2 = new ArrayList<>();
    list2.add(null);
    list2.add("");

    var ans1 = StreamApiDemo.sortedNonEmpty.apply(list1);
    var ans2 = StreamApiDemo.sortedNonEmpty.apply(list2);

    Assert.assertEquals(List.of("1", "2", "3"), ans1);
    Assert.assertEquals(List.of(), ans2);
  }

  @Test
  public void testReverseSorted() {
    Set<String> set = Set.of("1", "2", "3");

    var ans = StreamApiDemo.reverseSorted.apply(set);

    Assert.assertEquals(List.of("3", "2", "1"), ans);
  }

  @Test
  public void testSquaredSum() {
    Set<Integer> set = Set.of(-2, 1, 2, 3);

    var ans = StreamApiDemo.squaredSum.applyAsLong(set);

    Assert.assertEquals(18, ans);
  }

  @Test
  public void testMaxAgeAndSortedSexThenAge() {
    List<Human> list = new ArrayList<>();
    list.add(new Human("firstName", "lastName", "middleName", 23, Sex.FEMALE));
    list.add(new Human("firstName", "lastName", "middleName", 102, Sex.MALE));
    list.add(new Human("firstName", "lastName", "middleName", 18, Sex.MALE));
    list.add(new Human("firstName", "lastName", "middleName", 23, Sex.MALE));
    list.add(new Human("firstName", "lastName", "middleName", 14, Sex.FEMALE));
    list.add(new Human("firstName", "lastName", "middleName", 23, Sex.MALE));

    var ans1 = StreamApiDemo.maxAge.applyAsInt(list);
    var ans2 = StreamApiDemo.sortedSexThenAge.apply(list);

    List<Human> rightAns = new ArrayList<>();
    rightAns.add(new Human("firstName", "lastName", "middleName", 18, Sex.MALE));
    rightAns.add(new Human("firstName", "lastName", "middleName", 23, Sex.MALE));
    rightAns.add(new Human("firstName", "lastName", "middleName", 23, Sex.MALE));
    rightAns.add(new Human("firstName", "lastName", "middleName", 102, Sex.MALE));
    rightAns.add(new Human("firstName", "lastName", "middleName", 14, Sex.FEMALE));
    rightAns.add(new Human("firstName", "lastName", "middleName", 23, Sex.FEMALE));

    Assert.assertEquals(102, ans1);
    Assert.assertEquals(rightAns, ans2);
  }
}

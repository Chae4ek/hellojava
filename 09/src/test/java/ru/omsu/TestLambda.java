package ru.omsu;

import org.junit.Assert;
import org.junit.Test;

public class TestLambda {

  @Test
  public void testLength() {
    int demo1 = LambdaDemo.length.apply("123");
    int runner1 = LambdaRunner.run(LambdaDemo.length, "123");
    int demo2 = LambdaDemo.length.apply("");
    int runner2 = LambdaRunner.run(LambdaDemo.length, "");

    Assert.assertEquals(3, demo1);
    Assert.assertEquals(3, runner1);
    Assert.assertEquals(0, demo2);
    Assert.assertEquals(0, runner2);
  }

  @Test(expected = NullPointerException.class)
  public void testLengthNullDemo() {
    LambdaDemo.length.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testLengthNullRunner() {
    LambdaRunner.run(LambdaDemo.length, null);
  }

  @Test
  public void testFirstChar() {
    Character demo1 = LambdaDemo.firstChar.apply("123");
    Character runner1 = LambdaRunner.run(LambdaDemo.firstChar, "123");
    Character demo2 = LambdaDemo.firstChar.apply("");
    Character runner2 = LambdaRunner.run(LambdaDemo.firstChar, "");

    Assert.assertEquals(Character.valueOf('1'), demo1);
    Assert.assertEquals(Character.valueOf('1'), runner1);
    Assert.assertNull(demo2);
    Assert.assertNull(runner2);
  }

  @Test
  public void testNotContainsSpaces() {
    boolean demo1 = LambdaDemo.notContainsSpaces.apply("123");
    boolean runner1 = LambdaRunner.run(LambdaDemo.notContainsSpaces, "123");
    boolean demo2 = LambdaDemo.notContainsSpaces.apply("");
    boolean runner2 = LambdaRunner.run(LambdaDemo.notContainsSpaces, "");
    boolean demo3 = LambdaDemo.notContainsSpaces.apply("1 2 3");
    boolean runner3 = LambdaRunner.run(LambdaDemo.notContainsSpaces, "1 2 3");

    Assert.assertTrue(demo1);
    Assert.assertTrue(runner1);
    Assert.assertTrue(demo2);
    Assert.assertTrue(runner2);
    Assert.assertFalse(demo3);
    Assert.assertFalse(runner3);
  }

  @Test(expected = NullPointerException.class)
  public void testNotContainsSpacesNullDemo() {
    LambdaDemo.notContainsSpaces.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testNotContainsSpacesNullRunner() {
    LambdaRunner.run(LambdaDemo.notContainsSpaces, null);
  }

  @Test
  public void testWordsCount() {
    long demo1 = LambdaDemo.wordsCount.apply("123");
    long runner1 = LambdaRunner.run(LambdaDemo.wordsCount, "123");
    long demo2 = LambdaDemo.wordsCount.apply("");
    long runner2 = LambdaRunner.run(LambdaDemo.wordsCount, "");
    long demo3 = LambdaDemo.wordsCount.apply("1, 2,3");
    long runner3 = LambdaRunner.run(LambdaDemo.wordsCount, "1, 2,3");
    long demo4 = LambdaDemo.wordsCount.apply("1, 2,,,3");
    long runner4 = LambdaRunner.run(LambdaDemo.wordsCount, "1, 2,,,3");
    long demo5 = LambdaDemo.wordsCount.apply(",,,");
    long runner5 = LambdaRunner.run(LambdaDemo.wordsCount, ",,,");

    Assert.assertEquals(1, demo1);
    Assert.assertEquals(1, runner1);
    Assert.assertEquals(0, demo2);
    Assert.assertEquals(0, runner2);
    Assert.assertEquals(3, demo3);
    Assert.assertEquals(3, runner3);
    Assert.assertEquals(3, demo4);
    Assert.assertEquals(3, runner4);
    Assert.assertEquals(0, demo5);
    Assert.assertEquals(0, runner5);
  }

  @Test(expected = NullPointerException.class)
  public void testWordsCountNullDemo() {
    LambdaDemo.wordsCount.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testWordsCountNullRunner() {
    LambdaRunner.run(LambdaDemo.wordsCount, null);
  }

  @Test
  public void testYearsOld() {
    Human h1 = new Human("firstName", "lastName", "middleName", 123, Sex.MALE);
    Student h2 = new Student("university", "faculty", "specialty", "firstName", "lastName", "middleName", 321, Sex.MALE);

    int demo1 = LambdaDemo.yearsOld.apply(h1);
    int runner1 = LambdaRunner.run(LambdaDemo.yearsOld, h1);
    int demo2 = LambdaDemo.yearsOld.apply(h2);
    int runner2 = LambdaRunner.run(LambdaDemo.yearsOld, h2);

    Assert.assertEquals(123, demo1);
    Assert.assertEquals(123, runner1);
    Assert.assertEquals(321, demo2);
    Assert.assertEquals(321, runner2);
  }

  @Test(expected = NullPointerException.class)
  public void testYearsOldNullDemo() {
    LambdaDemo.yearsOld.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testYearsOldNullRunner() {
    LambdaRunner.run(LambdaDemo.yearsOld, null);
  }

  @Test
  public void testNamesakes() {
    Human h1 = new Human("firstName", "lastName", "middleName", 123, Sex.MALE);
    Student h2 = new Student("university", "faculty", "specialty", "firstName", "lastName", "middleName", 321, Sex.MALE);
    Human h3 = new Human("firstName", "ddd", "middleName", 0, Sex.MALE);

    boolean runner1 = LambdaRunner.run(LambdaDemo.namesakes, h1, h2);
    boolean runner2 = LambdaRunner.run(LambdaDemo.namesakes, h1, h3);

    Assert.assertTrue(runner1);
    Assert.assertFalse(runner2);
  }

  @Test(expected = NullPointerException.class)
  public void testNamesakesNullRunner() {
    LambdaRunner.run(LambdaDemo.namesakes, null, null);
  }

  @Test
  public void testFullName() {
    Human h1 = new Human("firstName", "lastName", "middleName", 123, Sex.MALE);
    Student h2 = new Student("university", "faculty", "specialty", "firstName", "lastName", "middleName", 321, Sex.MALE);

    String demo1 = LambdaDemo.fullName.apply(h1);
    String runner1 = LambdaRunner.run(LambdaDemo.fullName, h1);
    String demo2 = LambdaDemo.fullName.apply(h2);
    String runner2 = LambdaRunner.run(LambdaDemo.fullName, h2);

    Assert.assertEquals("lastName firstName middleName", demo1);
    Assert.assertEquals("lastName firstName middleName", runner1);
    Assert.assertEquals("lastName firstName middleName", demo2);
    Assert.assertEquals("lastName firstName middleName", runner2);
  }

  @Test(expected = NullPointerException.class)
  public void testFullNameNullDemo() {
    LambdaDemo.fullName.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testFullNameNullRunner() {
    LambdaRunner.run(LambdaDemo.fullName, null);
  }

  @Test
  public void testIncrementYearsOld() {
    Human h = new Human("firstName", "lastName", "middleName", 123, Sex.MALE);

    Human demo = LambdaDemo.incrementYearsOld.apply(h);
    Human runner = LambdaRunner.run(LambdaDemo.incrementYearsOld, h);

    final Human ans = new Human("firstName", "lastName", "middleName", 124, Sex.MALE);

    Assert.assertEquals(ans, demo);
    Assert.assertEquals(ans, runner);
  }

  @Test(expected = NullPointerException.class)
  public void testIncrementYearsOldNullDemo() {
    LambdaDemo.fullName.apply(null);
  }

  @Test(expected = NullPointerException.class)
  public void testIncrementYearsOldNullRunner() {
    LambdaRunner.run(LambdaDemo.fullName, null);
  }

  @Test
  public void testAllLessThan() {
    Human h1 = new Human("firstName", "lastName", "middleName", 123, Sex.MALE);
    Human h2 = new Human("firstName", "lastName", "middleName", 13, Sex.MALE);
    Human h3 = new Human("firstName", "lastName", "middleName", 5, Sex.MALE);
    Human h4 = new Human("firstName", "lastName", "middleName", 12000, Sex.MALE);

    boolean demo1 = LambdaDemo.allLessThan.apply(h1, h2, h3, 500);
    boolean runner1 = LambdaRunner.run(LambdaDemo.allLessThan, h1, h2, h3, 500);
    boolean demo2 = LambdaDemo.allLessThan.apply(h1, h4, h3, 500);
    boolean runner2 = LambdaRunner.run(LambdaDemo.allLessThan, h1, h4, h3, 500);

    Assert.assertTrue(demo1);
    Assert.assertTrue(runner1);
    Assert.assertFalse(demo2);
    Assert.assertFalse(runner2);
  }

  @Test(expected = NullPointerException.class)
  public void testAllLessThanNullDemo() {
    LambdaDemo.allLessThan.apply(null, null, null, 0);
  }

  @Test(expected = NullPointerException.class)
  public void testAllLessThanNullRunner() {
    LambdaRunner.run(LambdaDemo.allLessThan, null, null, null, 0);
  }
}

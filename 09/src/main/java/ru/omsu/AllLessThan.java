package ru.omsu;

import ru.omsu.Human;

@FunctionalInterface
public interface AllLessThan {

  boolean apply(Human h1, Human h2, Human h3, int maxAge);
}

package ru.omsu;

import java.util.Objects;

public class Human {

  private String firstName;
  private String lastName;
  private String middleName;
  private int yearsOld;
  private Sex sex;

  public Human(String firstName, String lastName, String middleName, int yearsOld, Sex sex) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.middleName = middleName;
    this.yearsOld = yearsOld;
    this.sex = sex;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public Sex getSex() {
    return sex;
  }

  public int getYearsOld() {
    return yearsOld;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setSex(Sex sex) {
    this.sex = sex;
  }

  public void setYearsOld(int yearsOld) {
    this.yearsOld = yearsOld;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Human other = (Human) obj;
    if (this.yearsOld != other.yearsOld) {
      return false;
    }
    if (!Objects.equals(this.firstName, other.firstName)) {
      return false;
    }
    if (!Objects.equals(this.lastName, other.lastName)) {
      return false;
    }
    if (!Objects.equals(this.middleName, other.middleName)) {
      return false;
    }
    return this.sex == other.sex;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 53 * hash + Objects.hashCode(this.firstName);
    hash = 53 * hash + Objects.hashCode(this.lastName);
    hash = 53 * hash + Objects.hashCode(this.middleName);
    hash = 53 * hash + this.yearsOld;
    hash = 53 * hash + Objects.hashCode(this.sex);
    return hash;
  }

}

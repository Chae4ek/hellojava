package ru.omsu;

import java.util.Objects;

public class Student extends Human {

  private String university;
  private String faculty;
  private String specialty;

  public Student(String university, String faculty, String specialty, String firstName, String lastName, String middleName, int yearsOld, Sex sex) {
    super(firstName, lastName, middleName, yearsOld, sex);
    this.university = university;
    this.faculty = faculty;
    this.specialty = specialty;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Student other = (Student) obj;
    if (!Objects.equals(this.university, other.university)) {
      return false;
    }
    if (!Objects.equals(this.faculty, other.faculty)) {
      return false;
    }
    return Objects.equals(this.specialty, other.specialty);
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 67 * hash + Objects.hashCode(this.university);
    hash = 67 * hash + Objects.hashCode(this.faculty);
    hash = 67 * hash + Objects.hashCode(this.specialty);
    return hash;
  }
}

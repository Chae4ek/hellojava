package ru.omsu;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class LambdaDemo {

  public static final Function<String, Integer> length = String::length;

  public static final Function<String, Character> firstChar = s -> s == null || s.length() == 0 ? null : s.charAt(0);

  public static final Function<String, Boolean> notContainsSpaces = s -> !s.contains(" ");

  public static final Function<String, Long> wordsCount = s -> s.length() == 0 ? 0 : Stream.of(s.split(","))
          .filter(str -> !str.equals(""))
          .count();

  public static final Function<Human, Integer> yearsOld = Human::getYearsOld;

  public static final BiPredicate<Human, Human> namesakes = (a, b)
          -> a.getLastName().equals(b.getLastName());

  public static final Function<Human, String> fullName = h
          -> String.join(" ", h.getLastName(), h.getFirstName(), h.getMiddleName());

  public static final Function<Human, Human> incrementYearsOld = h
          -> new Human(h.getFirstName(), h.getLastName(), h.getMiddleName(), h.getYearsOld() + 1, h.getSex());

  public static final AllLessThan allLessThan = (h1, h2, h3, maxAge)
          -> h1.getYearsOld() < maxAge
          && h2.getYearsOld() < maxAge
          && h3.getYearsOld() < maxAge;
}

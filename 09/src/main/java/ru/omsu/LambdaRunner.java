package ru.omsu;

import java.util.function.BiPredicate;
import java.util.function.Function;

public abstract class LambdaRunner {

  public static <T, U> U run(Function<T, U> lambda, T s) {
    return lambda.apply(s);
  }

  public static <T extends Human, U extends Human> boolean run(BiPredicate<T, U> lambda, T h1, U h2) {
    return lambda.test(h1, h2);
  }

  public static boolean run(AllLessThan lambda, Human h1, Human h2, Human h3, int maxAge) {
    return lambda.apply(h1, h2, h3, maxAge);
  }
}

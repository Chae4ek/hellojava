package ru.omsu;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class StreamApiDemo extends LambdaDemo {

  public static final UnaryOperator<List<?>> removeAllNull = l -> l.stream()
          .filter(Objects::nonNull)
          .collect(Collectors.toList());

  public static final ToLongFunction<Set<Integer>> positiveCount = s -> s.stream()
          .filter(c -> c > 0)
          .count();

  public static final UnaryOperator<List<?>> last3Elements = l -> l.stream()
          .skip(l.size() < 3 ? 0 : l.size() - 3)
          .collect(Collectors.toList());

  public static final Function<List<Integer>, Integer> firstEven = l -> l.stream()
          .filter(Objects::nonNull)
          .filter(c -> c % 2 == 0)
          .findFirst()
          .orElse(null);

  public static final Function<int[], List<Integer>> squaredListWithoutEquals = arr -> IntStream.of(arr)
          .map(i -> i * i)
          .distinct()
          .boxed()
          .collect(Collectors.toList());

  public static final UnaryOperator<List<String>> sortedNonEmpty = l -> l.stream()
          .filter(s -> s != null && !s.equals(""))
          .sorted()
          .collect(Collectors.toList());

  public static final Function<Set<String>, List<String>> reverseSorted = s -> s.stream()
          .sorted((s1, s2) -> s2.compareTo(s1))
          .collect(Collectors.toList());

  public static final ToLongFunction<Set<Integer>> squaredSum = s -> s.stream()
          .mapToLong(i -> i * i)
          .sum();

  public static final ToIntFunction<Collection<Human>> maxAge = c -> c.stream()
          .max((h1, h2) -> h1.getYearsOld() - h2.getYearsOld())
          .get()
          .getYearsOld();

  public static final UnaryOperator<Collection<Human>> sortedSexThenAge = c -> c.stream()
          .sorted((h1, h2) -> h1.getSex().compareTo(h2.getSex()) == 0
          ? h1.getYearsOld() - h2.getYearsOld() : h1.getSex().compareTo(h2.getSex()))
          .collect(Collectors.toList());
}

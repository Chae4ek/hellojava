package ru.omsu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PhoneBookTest {

  @Test
  public void test() {
    PhoneBook phonebook = new PhoneBook();
    Human h = new Human("null", "null", "null", 10);
    phonebook.addPhone(h, "88005553535");

    assertEquals("88005553535", phonebook.getPhoneList(h).get(0));
    assertEquals(h, phonebook.findByPhone("88005553535"));

    final PhoneBook ans = new PhoneBook();
    for (var phone : phonebook.getPhoneList(h)) {
      ans.addPhone(h, phone);
    }

    assertEquals(ans, phonebook.findAll("n"));
    assertEquals(new PhoneBook(), phonebook.findAll("d"));
  }
}

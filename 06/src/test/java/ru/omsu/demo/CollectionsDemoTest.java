package ru.omsu.demo;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import ru.omsu.Human;

public class CollectionsDemoTest {
  @Test
  public void test() {
    List<String> ss = new LinkedList<String>();
    ss.add("asd");
    ss.add("sd");
    ss.add("asddf");

    assertEquals(2, CollectionsDemo.countStringWithFirstChar(ss, 'a'));

    Human a = new Human("null", "null", "null", 10);
    Human b = new Human("null", "null1", "null", 101);
    Human c = new Human("nul2l", "null", "null", 1220);
    List<Human> hs = new LinkedList<Human>();
    hs.add(a);
    hs.add(b);
    hs.add(c);

    LinkedList<Human> ans = new LinkedList<Human>();
    ans.add(b);
    ans.add(c);

    assertEquals(ans, CollectionsDemo.allWithoutOne(hs, a));

    List<Set<Integer>> all = new LinkedList<Set<Integer>>();
    LinkedHashSet<Integer> A = new LinkedHashSet<Integer>();
    A.add(123);
    A.add(321);
    LinkedHashSet<Integer> B = new LinkedHashSet<Integer>();
    B.add(4);
    B.add(32);
    B.add(22);
    all.add(A);
    all.add(B);
    LinkedHashSet<Integer> one = new LinkedHashSet<Integer>();
    one.add(5);
    one.add(22);
    one.add(2);
    one.add(4);
    one.add(9);

    final LinkedList<Set<Integer>> ANS = new LinkedList<Set<Integer>>();
    ANS.add(A);

    assertEquals(ANS, CollectionsDemo.allNotIntersectedWithOne(all, one));
  }
}

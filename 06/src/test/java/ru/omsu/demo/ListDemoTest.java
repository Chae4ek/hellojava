package ru.omsu.demo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import ru.omsu.Human;

public class ListDemoTest {
  @Test
  public void test() {
    Human a1 = new Human("firssdft", "secosnd", "midddle", 10);
    Human a2 = new Human("firsdt", "asdd", "middle", 18);
    Human a3 = new Human("5f4", "sfecond", "asddd", 19);
    Human a4 = new Human("first", "secdsond", "middle", 12);
    Human a5 = new Human("first", "asdd", "middle", 10);
    Human a6 = new Human("54ffk", "dkfgk", "ffkfgkk", 19);

    List<Human> hs = new LinkedList<Human>();
    hs.add(a1);
    hs.add(a2);
    hs.add(a3);
    hs.add(a4);
    hs.add(a5);
    hs.add(a6);

    Human one = new Human("first", "asdd", "middle", 10);

    final ArrayList<Human> ans = new ArrayList<Human>();
    ans.add(a2);
    ans.add(a5);

    assertEquals(ans, ListDemo.findNamesakesWithOne(hs, one));

    ArrayList<Human> all = new ArrayList<Human>();
    all.add(a1);
    all.add(a2);
    all.add(a3);
    all.add(a4);
    all.add(a5);
    all.add(a6);

    final LinkedHashSet<Human> ans2 = new LinkedHashSet<Human>();
    ans2.add(a3);
    ans2.add(a6);

    assertEquals(ans2, ListDemo.peopleWithMaxAge(all));

    Map<Integer, Human> people = new HashMap<>();
    Set<Integer> IDs = new HashSet<Integer>();
    people.put(1, a1);
    people.put(2, a2);
    people.put(3, a3);
    people.put(4, a4);
    people.put(5, a5);
    people.put(6, a6);
    IDs.add(2);
    IDs.add(4);
    IDs.add(5);

    final HashSet<Human> ans3 = new HashSet<Human>();
    ans3.add(a2);
    ans3.add(a4);
    ans3.add(a5);

    assertEquals(ans3, ListDemo.findPeopleByID(people, IDs));

    final LinkedList<Integer> ans4 = new LinkedList<Integer>();
    ans4.add(2);
    ans4.add(3);
    ans4.add(6);

    assertEquals(ans4, ListDemo.getAdultsIDs(people));

    final Map<Integer, Integer> ans5 = new LinkedHashMap<Integer, Integer>();
    ans5.put(1, 10);
    ans5.put(2, 18);
    ans5.put(3, 19);
    ans5.put(4, 12);
    ans5.put(5, 10);
    ans5.put(6, 19);

    assertEquals(ans5, ListDemo.IDsToAges(people));

    Set<Human> people2 = new HashSet<Human>();
    people2.add(a1);
    people2.add(a2);
    people2.add(a3);
    people2.add(a4);
    people2.add(a5);
    people2.add(a6);

    final LinkedHashMap<Integer, LinkedList<Human>> ans6 = new LinkedHashMap<Integer, LinkedList<Human>>();
    LinkedList<Human> l1 = new LinkedList<Human>();
    l1.add(a1);
    l1.add(a5);
    LinkedList<Human> l2 = new LinkedList<Human>();
    l2.add(a4);
    LinkedList<Human> l3 = new LinkedList<Human>();
    l3.add(a2);
    LinkedList<Human> l4 = new LinkedList<Human>();
    l4.add(a6);
    l4.add(a3);
    ans6.put(10, l1);
    ans6.put(12, l2);
    ans6.put(19, l4);
    ans6.put(18, l3);

    assertEquals(ans6, ListDemo.agesToPeople(people2));
  }

  @Test
  public void testSort2() {
    Human a1 = new Human("firssdft", "asecosnd", "midddle", 10);
    Human a2 = new Human("firsdt", "asd", "middle", 18);
    Human a3 = new Human("5f4", "sfecond", "asddd", 19);
    Human a4 = new Human("first", "secdsond", "middle", 12);
    Human a5 = new Human("first", "asdd", "middle", 10);
    Human a6 = new Human("54ffk", "dkfgk", "ffkfgkk", 19);

    Set<Human> people2 = new HashSet<Human>();
    people2.add(a1);
    people2.add(a2);
    people2.add(a3);
    people2.add(a4);
    people2.add(a5);
    people2.add(a6);

    LinkedList<Human> l1 = new LinkedList<Human>();
    l1.add(a5);
    l1.add(a1);
    LinkedList<Human> l2 = new LinkedList<Human>();
    l2.add(a4);
    LinkedList<Human> l3 = new LinkedList<Human>();
    l3.add(a2);

    final LinkedHashMap<Integer, LinkedHashMap<Character, LinkedList<Human>>> ans7 = new LinkedHashMap<Integer, LinkedHashMap<Character, LinkedList<Human>>>();
    LinkedHashMap<Character, LinkedList<Human>> m1 = new LinkedHashMap<Character, LinkedList<Human>>();
    m1.put('a', l1);
    LinkedHashMap<Character, LinkedList<Human>> m2 = new LinkedHashMap<Character, LinkedList<Human>>();
    m2.put('s', l2);
    LinkedHashMap<Character, LinkedList<Human>> m3 = new LinkedHashMap<Character, LinkedList<Human>>();
    m3.put('a', l3);
    LinkedHashMap<Character, LinkedList<Human>> m4 = new LinkedHashMap<Character, LinkedList<Human>>();
    LinkedList<Human> l6 = new LinkedList<Human>();
    l6.add(a3);
    LinkedList<Human> l7 = new LinkedList<Human>();
    l7.add(a6);
    m4.put('d', l7);
    m4.put('s', l6);
    ans7.put(18, m3);
    ans7.put(12, m2);
    ans7.put(10, m1);
    ans7.put(19, m4);

    assertEquals(ans7, ListDemo.getSortedAgeToFirstLetterOfSecondName(people2));
  }

  @Test
  public void testSort() {
    Set<Human> set = new HashSet<Human>();
    set.add(new Human("3", "null", "null", 0));
    set.add(new Human("1", "null", "null", 0));
    set.add(new Human("2", "null", "null", 0));

    List<Human> sort = ListDemo.toSort(set);

    final List<Human> ans = new LinkedList<Human>();
    ans.add(new Human("1", "null", "null", 0));
    ans.add(new Human("2", "null", "null", 0));
    ans.add(new Human("3", "null", "null", 0));

    assertEquals(ans, sort);

    Human a1 = new Human("firssdft", "secosnd", "midddle", 10);
    Human a2 = new Human("firsdt", "asd", "middle", 18);
    Human a3 = new Human("5f4", "sfecond", "asddd", 19);
    Human a4 = new Human("first", "secdsond", "middle", 12);
    Human a5 = new Human("first", "asdd", "middle", 10);
    Human a6 = new Human("54ffk", "dkfgk", "ffkfgkk", 19);

    HashSet<Human> set2 = new HashSet<Human>();
    set2.add(a1);
    set2.add(a2);
    set2.add(a3);
    set2.add(a4);
    set2.add(a5);
    set2.add(a6);

    final LinkedList<Human> ans3 = new LinkedList<Human>();
    ans3.add(a6);
    ans3.add(a3);
    ans3.add(a2);
    ans3.add(a1);
    ans3.add(a5);
    ans3.add(a4);

    assertEquals(ans3, ListDemo.toSort(set2));
  }
}

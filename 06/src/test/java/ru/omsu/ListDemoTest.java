package ru.omsu;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import ru.omsu.demo.ListDemo;

public class ListDemoTest {
  @Test
  public void test() {
    Set<Human> set = new HashSet<Human>();
    set.add(new Human("3", "null", "null", 0));
    set.add(new Human("1", "null", "null", 0));
    set.add(new Human("2", "null", "null", 0));

    List<Human> sort = ListDemo.toSort(set);

    final List<Human> ans = new LinkedList<Human>();
    ans.add(new Human("1", "null", "null", 0));
    ans.add(new Human("2", "null", "null", 0));
    ans.add(new Human("3", "null", "null", 0));

    assertEquals(ans, sort);
  }
}

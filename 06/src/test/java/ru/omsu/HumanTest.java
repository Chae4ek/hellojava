package ru.omsu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HumanTest {
  @Test
  public void test() {
    Human h1 = new Human("first name", "second name", "middle name", 20);
    Human h2 = new Human("first name", "second name", "middle name", 20);
    Human h3 = new Human("n", "n", "n", 19);

    assertEquals(true, h1.equals(h2));
    assertEquals(true, h2.equals(h1));
    assertEquals(false, h1.equals(h3));
    assertEquals(false, h3.equals(h1));
  }
}

package ru.omsu;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DataTest {

  @Test
  public void test() {
    Group g1 = new Group(1, 0, 1, 2);
    Group g2 = new Group(2, 3, 4, 5);
    Data d = new Data("groups", g1, new Group(3), g2, new Group(100));

    boolean[] all = new boolean[6];

    for (var i : d) {
      System.err.println(i);
      all[i] = true;
    }

    for (boolean i : all) {
      System.err.println(i);
      assertEquals(true, i);
    }
  }

  @Test
  public void test2() {
    Data d = new Data("asd", new Group(3), new Group(100));

    var f = d.iterator();
    assertEquals(false, f.hasNext());
    int i = 0;
    for (var g : d) {
      ++i;
    }
    assertEquals(0, i);
  }

  @Test
  public void test3() {
    Group g1 = new Group(1, 0, 1, 2);
    Group g2 = new Group(2, 3, 4, 5);
    Data d = new Data("groups", g1, new Group(3), g2);

    boolean[] all = new boolean[6];

    for (var i : d) {
      System.err.println(i);
      all[i] = true;
    }

    for (boolean i : all) {
      System.err.println(i);
      assertEquals(true, i);
    }
  }
}

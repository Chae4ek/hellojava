package ru.omsu;

public class Student extends Human {
  private String faculty;

  public Student(String firstName, String secondName, String middleName, int age, String faculty) {
    super(firstName, secondName, middleName, age);
    this.faculty = faculty;
  }

  public String getFaculty() {
    return faculty;
  }

  public void setFaculty(String faculty) {
    this.faculty = faculty;
  }
}

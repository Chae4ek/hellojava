package ru.omsu.demo;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ru.omsu.Human;

public class CollectionsDemo {
  public static int countStringWithFirstChar(List<String> strs, char c) {
    int count = 0;
    for (String s : strs) {
      if (s != null && s.charAt(0) == c) {
        ++count;
      }
    }
    return count;
  }

  public static List<Human> allWithoutOne(List<Human> people, Human one) {
    List<Human> allWithoutOne = new LinkedList<Human>();
    for (Human h : people) {
      if (!h.equals(one)) {
        allWithoutOne.add(new Human(h.getFirstName(), h.getSecondName(), h.getMiddleName(), h.getAge()));
      }
    }
    return allWithoutOne;
  }

  public static List<Set<Integer>> allNotIntersectedWithOne(List<Set<Integer>> all, Set<Integer> one) {
    List<Set<Integer>> allNotIntersectedWithOne = new LinkedList<Set<Integer>>();
    for (var set : all) {
      Set<Integer> addition = new LinkedHashSet<Integer>(set);
      if (!addition.removeAll(one)) {
        allNotIntersectedWithOne.add(set);
      }
    }
    return allNotIntersectedWithOne;
  }
}

package ru.omsu.demo;

import java.util.LinkedList;
import java.util.List;

import ru.omsu.Data;

public class DataDemo {
  public static List<Integer> getAll(Data data) {
    List<Integer> list = new LinkedList<Integer>();
    for (var i : data) {
      list.add(i);
    }
    return list;
  }
}

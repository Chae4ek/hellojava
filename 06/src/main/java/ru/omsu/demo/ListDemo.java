package ru.omsu.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import ru.omsu.Human;

public class ListDemo {

  public static List<Human> findNamesakesWithOne(List<Human> people, Human one) {
    List<Human> namesakes = new ArrayList<Human>();
    for (Human h : people) {
      if (h.getSecondName().equals(one.getSecondName())) {
        namesakes.add(h);
      }
    }
    return namesakes;
  }

  public static <T extends Human> Set<T> peopleWithMaxAge(List<T> all) {
    Set<T> peopleWithMaxAge = new LinkedHashSet<T>();
    var maxAge = -1;
    for (T h : all) {
      if (h.getAge() > maxAge) {
        maxAge = h.getAge();
        peopleWithMaxAge.clear();
        peopleWithMaxAge.add(h);
      } else if (h.getAge() == maxAge) {
        peopleWithMaxAge.add(h);
      }
    }
    return peopleWithMaxAge;
  }

  private static int compareNames(Human h1, Human h2) {
    int comp = h1.getFirstName().compareTo(h2.getFirstName());
    if (comp == 0) {
      comp = h1.getSecondName().compareTo(h2.getSecondName());
      if (comp == 0) {
        comp = h1.getMiddleName().compareTo(h2.getMiddleName());
      }
    }
    return comp;
  }

  public static <T extends Human> List<T> toSort(Set<T> set) {
    List<T> sort = new LinkedList<T>();
    for (var s : set) {
      int i = 0;
      for (; i < sort.size(); ++i) {
        if (compareNames(sort.get(i), s) >= 0) {
          break;
        }
      }
      sort.add(i, s);
    }
    return sort;
  }

  public static Set<Human> findPeopleByID(Map<Integer, Human> people, Set<Integer> IDs) {
    Set<Human> set = new HashSet<Human>();
    for (int id : IDs) {
      if (people.get(id) != null) {
        set.add(people.get(id));
      }
    }
    return set;
  }

  /**
   * @return list of 18+ people IDs
   */
  public static List<Integer> getAdultsIDs(Map<Integer, Human> people) {
    List<Integer> IDs = new LinkedList<Integer>();
    people.forEach((k, v) -> {
      if (v.getAge() >= 18) {
        IDs.add(k);
      }
    });
    return IDs;
  }

  public static Map<Integer, Integer> IDsToAges(Map<Integer, Human> people) {
    Map<Integer, Integer> map = new LinkedHashMap<Integer, Integer>();
    people.forEach((k, v) -> map.put(k, v.getAge()));
    return map;
  }

  public static Map<Integer, List<Human>> agesToPeople(Set<Human> people) {
    Map<Integer, List<Human>> map = new LinkedHashMap<Integer, List<Human>>();
    for (var v : people) {
      if (map.get(v.getAge()) == null) {
        map.put(v.getAge(), new LinkedList<Human>());
      }
      map.get(v.getAge()).add(v);
    }
    return map;
  }

  public static Map<Integer, Map<Character, List<Human>>> getSortedAgeToFirstLetterOfSecondName(Set<Human> people) {
    Map<Integer, Map<Character, List<Human>>> map = new LinkedHashMap<Integer, Map<Character, List<Human>>>();
    var agesToPeople = agesToPeople(people);
    for (var p : agesToPeople.entrySet()) {
      var map2 = new LinkedHashMap<Character, List<Human>>();
      map.put(p.getKey(), map2);

      for (var human : p.getValue()) {
        var list = map2.get(human.getSecondName().charAt(0));
        if (list == null) {
          list = new LinkedList<Human>();
          map2.put(human.getSecondName().charAt(0), list);
        }

        // Sort
        int i = 0;
        for (; i < list.size(); ++i) {
          if (compareNames(list.get(i), human) <= 0) {
            break;
          }
        }
        list.add(i, human);
      }
    }
    return map;
  }
}

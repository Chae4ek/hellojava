package ru.omsu;

public class Human {
  private String firstName;
  private String secondName;
  private String middleName;
  private int age;

  public Human(String firstName, String secondName, String middleName, int age) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.middleName = middleName;
    this.age = age;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public int getAge() {
    return age;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    Human h = (Human) obj;

    return age == h.age && firstName.equals(h.firstName) && secondName.equals(h.secondName)
        && middleName.equals(h.middleName);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + firstName.hashCode();
    result = prime * result + secondName.hashCode();
    result = prime * result + middleName.hashCode();
    result = prime * result + age;
    return result;
  }
}

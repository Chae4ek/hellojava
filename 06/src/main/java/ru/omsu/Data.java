package ru.omsu;

import java.util.Iterator;

public class Data implements Iterable<Integer> {

  private String name;
  private Group[] groups;

  public Data(String name, Group... groups) {
    this.name = name;
    this.groups = groups;
  }

  public String getName() {
    return name;
  }

  public Group[] getGroups() {
    return groups;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setGroups(Group[] groups) {
    this.groups = groups;
  }

  public int getLength() {
    return groups.length;
  }

  @Override
  public Iterator<Integer> iterator() {
    return new Iterator<Integer>() {
      private int currGoupIndex = 0;
      private int currDataIndex = 0;

      @Override
      public boolean hasNext() {
        while (currGoupIndex < groups.length && currDataIndex == groups[currGoupIndex].getLength()) {
          currDataIndex = 0;
          ++currGoupIndex;
        }
        return currGoupIndex < groups.length;
      }

      @Override
      public Integer next() {
        int thisIndex = currDataIndex++;
        int thisGroup = currGoupIndex;
        if (currDataIndex == groups[currGoupIndex].getLength()) {
          currDataIndex = 0;
          ++currGoupIndex;
        }
        return groups[thisGroup].getData()[thisIndex];
      }
    };
  }
}

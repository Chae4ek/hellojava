package ru.omsu;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PhoneBook {

  private final Map<Human, List<String>> book;

  public PhoneBook() {
    book = new LinkedHashMap<>();
  }

  public void addPhone(Human h, String phone) {
    List<String> list = book.get(h);
    if (list == null) {
      list = new LinkedList<>();
      book.put(h, list);
    }
    list.add(phone);
  }

  public void removePhone(Human h, String phone) {
    book.get(h).remove(phone);
  }

  public List<String> getPhoneList(Human h) {
    return book.get(h);
  }

  public Human findByPhone(String phone) {
    Human h = null;
    for (var b : book.entrySet()) {
      if (book.get(b.getKey()).contains(phone)) {
        h = b.getKey();
        break;
      }
    }
    return h;
  }

  public PhoneBook findAll(String secondNameStarts) {
    PhoneBook newBook = new PhoneBook();
    for (var b : book.entrySet()) {
      if (b.getKey().getSecondName().startsWith(secondNameStarts)) {
        for (var phone : b.getValue()) {
          newBook.addPhone(b.getKey(), phone);
        }
      }
    }
    return newBook;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    PhoneBook b = (PhoneBook) obj;

    return b.book.equals(this.book);
  }

}

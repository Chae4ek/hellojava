package ru.omsu;

public class SquareTrinomial {
  private double a;
  private double b;
  private double c;

  /**
   * Creates {@code a*x^2 + b*x + c}
   *
   * @throws IllegalArgumentException if the trinomial is not square
   *                                  ({@code a == 0})
   */
  public SquareTrinomial(double a, double b, double c) throws IllegalArgumentException {
    if (a == 0) {
      throw new IllegalArgumentException("The equation is not square");
    }
    this.a = a;
    this.b = b;
    this.c = c;
  }

  public double getA() {
    return a;
  }

  public double getB() {
    return b;
  }

  public double getC() {
    return c;
  }

  /**
   * @throws IllegalArgumentException if {@code a == 0}
   */
  public void setA(double a) throws IllegalArgumentException {
    if (a == 0) {
      throw new IllegalArgumentException("The equation is not square");
    }
    this.a = a;
  }

  public void setB(double b) {
    this.b = b;
  }

  public void setC(double c) {
    this.c = c;
  }

  /**
   * Solves the following quadratic equation: {@code a*x^2 + b*x + c = 0}
   *
   * @return array of roots or null if the equation has no real roots (it can also
   *         return an array with two equal roots)
   * @throws IllegalArgumentException if the equation is not square
   *                                  ({@code a == 0})
   */
  public double[] solveQuadraticEquation() throws IllegalArgumentException {
    final double discriminant = b * b - 4 * a * c;

    if (a == 0) {
      throw new IllegalArgumentException("The equation is not square");
    }

    if (discriminant < 0) {
      return null;
    } else if (discriminant == 0) {
      final double root = -b / 2 / a;
      return new double[] { root, root };
    } else {
      return new double[] { (-b + Math.sqrt(discriminant)) / 2 / a, (-b - Math.sqrt(discriminant)) / 2 / a };
    }
  }
}

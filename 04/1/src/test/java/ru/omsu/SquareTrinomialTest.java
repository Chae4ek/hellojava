package ru.omsu;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class SquareTrinomialTest {
  @Test
  public void test() {
    assertArrayEquals(new double[] { -1, -1 }, new SquareTrinomial(1, 2, 1).solveQuadraticEquation(), 1e-10);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testException() {
    new SquareTrinomial(0, 2, 3);
  }
}

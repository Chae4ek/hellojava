package ru.omsu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OperationsTest {
  @Test
  public void test() {
    Operations op = new Operations(new SquareTrinomial(1, -3, 2));

    // roots = {1, 2}
    assertEquals(2, op.getMaxRoot(), 1e-10);
  }

  @Test(expected = NullPointerException.class)
  public void testException() {
    Operations op = new Operations(new SquareTrinomial(1, 2, 3));

    op.getMaxRoot();
  }
}

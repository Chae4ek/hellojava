package ru.omsu.Functionality;

import ru.omsu.Function.IFunction;

public interface IFunctional<T extends IFunction> {
  public double getValue(T function);
}

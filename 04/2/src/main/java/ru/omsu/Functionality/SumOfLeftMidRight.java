package ru.omsu.Functionality;

import ru.omsu.Function.IFunction;

public class SumOfLeftMidRight<T extends IFunction> implements IFunctional<T> {

  private double left;
  private double right;

  /**
   * @throws IllegalArgumentException if values out of bounds on which the
   *                                  function is defined
   */
  public SumOfLeftMidRight(double left, double right) throws IllegalArgumentException {
    if (left > right) {
      throw new IllegalArgumentException("Left value more than right value");
    }
    this.left = left;
    this.right = right;
  }

  /**
   * @return sum of left, right and (left + right)/2 values
   */
  @Override
  public double getValue(T function) {
    if (function == null) {
      throw new IllegalArgumentException("Function is null");
    }
    if (left > right || left < function.getLeftBorder() || right > function.getRightBorder()) {
      throw new IllegalArgumentException("Values out of bounds on which the function is defined");
    }
    return function.getValue(left) + function.getValue(right) + function.getValue((left + right) / 2);
  }
}

package ru.omsu.Function;

public interface IFunction {
  /**
   * @return left boundary of segment on which the function is defined
   */
  public double getLeftBorder();

  /**
   * @return right boundary of segment on which the function is defined
   */
  public double getRightBorder();

  /**
   * @return value of the function
   */
  public double getValue(double x);

  /**
   * @return true if function is definited on [a, b]
   */
  public boolean isDefinite(double a, double b);
}

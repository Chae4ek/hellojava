package ru.omsu;

public class Operations {
  private SquareTrinomial st;

  /**
   * @throws NullPointerException if st is null
   */
  public Operations(SquareTrinomial st) throws NullPointerException {
    if (st == null) {
      throw new NullPointerException("Square trinomial is null");
    }
    this.st = st;
  }

  /**
   * @return maximum of roots
   * @throws NullPointerException if trinomial is null or roots are not found
   */
  public double getMaxRoot() throws NullPointerException {
    if (st == null) {
      throw new NullPointerException("Square trinomial is null");
    }
    double[] roots = st.solveQuadraticEquation();
    if (roots == null) {
      throw new NullPointerException("Roots are not found");
    }
    return Math.max(roots[0], roots[1]);
  }
}

package ru.omsu;

import java.util.Comparator;

import ru.omsu.product.products.Product;

public class ProductComparator implements Comparator<Product> {
  public ProductComparator() {
  }

  @Override
  public int compare(Product o1, Product o2) {
    if (o1.getName() == o2.getName()) {
      StringBuilder sb1 = new StringBuilder();
      for (String s : o1.getDescription()) {
        sb1.append(s);
      }
      StringBuilder sb2 = new StringBuilder();
      for (String s : o2.getDescription()) {
        sb2.append(s);
      }

      return sb1.compareTo(sb2);
    }

    return o1.getName().compareTo(o2.getName());
  }
}

package ru.omsu;

import java.util.Arrays;
import java.util.Comparator;

import ru.omsu.product.products.Product;

public class ComparatorDemo {
  public static void sortGoods(Product[] goods, Comparator<Product> comp) {
    Arrays.sort(goods, comp);
  }
}

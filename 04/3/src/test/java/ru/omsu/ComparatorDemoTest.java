package ru.omsu;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import ru.omsu.product.products.Product;
import ru.omsu.product.products.WeightProduct;

public class ComparatorDemoTest {
  @Test
  public void test() {
    Product p1 = new WeightProduct("1", new String[] {});
    Product p2 = new WeightProduct("2", new String[] {});
    Product p3 = new WeightProduct("3", new String[] {});

    Product[] goods = new Product[] { p2, p3, p1 };
    ProductComparator comp = new ProductComparator();

    ComparatorDemo.sortGoods(goods, comp);
    assertArrayEquals(new Product[] { p1, p2, p3 }, goods);
  }
}

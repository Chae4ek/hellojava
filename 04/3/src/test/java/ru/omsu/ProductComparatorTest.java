package ru.omsu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.product.products.Product;
import ru.omsu.product.products.WeightProduct;

public class ProductComparatorTest {
  @Test
  public void test() {
    Product p1 = new WeightProduct("1 name", new String[] { "description" });
    Product p2 = new WeightProduct("2 name", new String[] { "1 description" });
    Product p3 = new WeightProduct("2 name", new String[] { "2 description" });
    Product p4 = new WeightProduct("1 name", new String[] { "description" });
    Product p5 = new WeightProduct("1 name", new String[] {});

    ProductComparator comp = new ProductComparator();

    assertEquals(true, comp.compare(p1, p2) < 0);
    assertEquals(true, comp.compare(p2, p1) > 0);
    assertEquals(true, comp.compare(p2, p3) < 0);
    assertEquals(true, comp.compare(p1, p4) == 0);
    assertEquals(true, comp.compare(p4, p5) > 0);
  }
}

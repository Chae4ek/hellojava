package ru.omsu.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OnlyNumbersFilterTest {
  @Test
  public void test() {
    OnlyNumbersFilter filter = new OnlyNumbersFilter();

    assertEquals(false, filter.apply(null));
    assertEquals(false, filter.apply(""));
    assertEquals(false, filter.apply("asd"));
    assertEquals(false, filter.apply("123d"));
    assertEquals(false, filter.apply("a123"));
    assertEquals(true, filter.apply("123"));
  }
}

package ru.omsu.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ContainsFilterTest {
  @Test
  public void test() {
    ContainsFilter filter = new ContainsFilter("asd");

    assertEquals(true, filter.apply("fdsawdadsasd"));
    assertEquals(false, filter.apply("fdsawdadsas"));
  }
}

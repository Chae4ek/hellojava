package ru.omsu.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BeginStringFilterTest {
  @Test
  public void test() {
    String str = "Мама мыла раму";
    BeginStringFilter filter1 = new BeginStringFilter("Мама");
    BeginStringFilter filter2 = new BeginStringFilter("мыла");

    assertEquals(true, filter1.apply(str));
    assertEquals(false, filter2.apply(str));
  }
}

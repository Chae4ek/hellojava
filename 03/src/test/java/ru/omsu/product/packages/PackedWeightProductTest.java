package ru.omsu.product.packages;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.product.products.ProductBox;
import ru.omsu.product.products.WeightProduct;

public class PackedWeightProductTest {
  @Test
  public void getters() {
    ProductBox pb = new ProductBox("name box", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb, wp, 4);

    assertEquals(pb, pwp.getProductBox());
    assertEquals(wp, pwp.getProduct());
    assertEquals(4, pwp.getMass());
    assertEquals(4, pwp.getNetto());
    assertEquals(5, pwp.getBrutto());
  }

  @Test
  public void strings() {
    ProductBox pb = new ProductBox("name box", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb, wp, 4);

    assertEquals(
        "Product box:\nName: name box, mass: 1\nMass: 4, weight product:\nName: name, description:\n\tdescription\nThis is weight product.",
        pwp.toString());
  }

  @Test
  public void equals() {
    ProductBox pb = new ProductBox("name box", 1);
    ProductBox pb2 = new ProductBox("name box2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });

    PackedWeightProduct a = new PackedWeightProduct(pb, wp, 4);
    PackedWeightProduct b = new PackedWeightProduct(pb, wp, 4);
    PackedWeightProduct c = new PackedWeightProduct(pb, wp, 5);
    PackedWeightProduct d = new PackedWeightProduct(pb2, wp, 4);

    assertEquals(a, b);
    assertEquals(b, a);
    assertEquals(a, a);
    assertEquals(a.hashCode(), b.hashCode());
    assertEquals(false, a.hashCode() == c.hashCode());
    assertEquals(false, a.equals(c));
    assertEquals(false, c.equals(a));
    assertEquals(false, a.equals(d));
    assertEquals(false, d.equals(a));
    assertEquals(false, c.equals(d));
    assertEquals(false, d.equals(c));
  }
}

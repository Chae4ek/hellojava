package ru.omsu.product.packages;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.product.products.PieceProduct;
import ru.omsu.product.products.ProductBox;
import ru.omsu.product.products.WeightProduct;

public class ProductSetTest {
  @Test
  public void getters() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);
    ProductBox setBox2 = new ProductBox("set box 2", 4);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);
    ProductSet set2 = new ProductSet(setBox2, set1);

    assertEquals(new ProductBox("set box 1", 4), set1.getProductBox());
    assertEquals(46, set1.getNetto());
    assertEquals(50, set1.getBrutto());
    assertEquals(set2.getNetto(), set1.getBrutto());
    assertEquals(54, set2.getBrutto());
    assertEquals(new ProductBox("set box 2", 4), set2.getProductBox());

    final ProductBox _pb1 = new ProductBox("name box 1", 1);
    final PieceProduct _pp = new PieceProduct("name", new String[] { "description" }, 10);
    final PackedPieceProduct _ppp = new PackedPieceProduct(_pb1, _pp, 4);
    final ProductBox _pb2 = new ProductBox("name box 2", 1);
    final WeightProduct _wp = new WeightProduct("name", new String[] { "description" });
    final PackedWeightProduct _pwp = new PackedWeightProduct(_pb2, _wp, 4);
    final ProductBox _setBox1 = new ProductBox("set box 1", 4);
    final ProductSet _set1 = new ProductSet(_setBox1, _ppp, _pwp);

    assertArrayEquals(new IPackedProductSetable[] { _ppp, _pwp }, set1.getProducts());
    assertArrayEquals(new IPackedProductSetable[] { _set1 }, set2.getProducts());
  }

  @Test
  public void strings() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);
    ProductBox setBox2 = new ProductBox("set box 2", 3);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);
    ProductSet set2 = new ProductSet(setBox2, set1);

    assertEquals(
        "Product box:\nName: set box 1, mass: 4\nPacked product set:\nProduct box:\nName: name box 1, mass: 1\nCount: 4, piece product:\nName: name, description:\n\tdescription\nMass of one: 10\nProduct box:\nName: name box 2, mass: 1\nMass: 4, weight product:\nName: name, description:\n\tdescription\nThis is weight product.",
        set1.toString());
    assertEquals(
        "Product box:\nName: set box 2, mass: 3\nPacked product set:\nProduct box:\nName: set box 1, mass: 4\nPacked product set:\nProduct box:\nName: name box 1, mass: 1\nCount: 4, piece product:\nName: name, description:\n\tdescription\nMass of one: 10\nProduct box:\nName: name box 2, mass: 1\nMass: 4, weight product:\nName: name, description:\n\tdescription\nThis is weight product.",
        set2.toString());
  }

  @Test
  public void equals() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);
    ProductBox setBox2 = new ProductBox("set box 2", 4);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);
    ProductSet set2 = new ProductSet(setBox2, set1);

    final ProductBox _pb1 = new ProductBox("name box 1", 1);
    final PieceProduct _pp = new PieceProduct("name", new String[] { "description" }, 10);
    final PackedPieceProduct _ppp = new PackedPieceProduct(_pb1, _pp, 4);
    final ProductBox _pb2 = new ProductBox("name box 2", 1);
    final WeightProduct _wp = new WeightProduct("name", new String[] { "description" });
    final PackedWeightProduct _pwp = new PackedWeightProduct(_pb2, _wp, 4);
    final ProductBox _setBox1 = new ProductBox("set box 1", 4);
    final ProductBox _setBox2 = new ProductBox("set box 2", 4);
    final ProductSet _set1 = new ProductSet(_setBox1, _ppp, _pwp);
    final ProductSet _set2 = new ProductSet(_setBox2, _set1);

    assertEquals(set1, set1);
    assertEquals(set2, set2);
    assertEquals(_set1, set1);
    assertEquals(_set2, set2);

    assertEquals(_set1.hashCode(), set1.hashCode());
    assertEquals(_set2.hashCode(), set2.hashCode());
    assertEquals(false, set1.hashCode() == set2.hashCode());
    assertEquals(false, set1.equals(set2));
    assertEquals(false, set2.equals(set1));

    ProductBox pb1_c = new ProductBox("name box 1", 1);
    PieceProduct pp_c = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp_c = new PackedPieceProduct(pb1_c, pp_c, 4);

    ProductBox pb2_c = new ProductBox("name box 2", 1);
    WeightProduct wp_c = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp_c = new PackedWeightProduct(pb2_c, wp_c, 4);

    ProductBox setBox1_c = new ProductBox("set box 1", 4);

    ProductSet set1_c = new ProductSet(setBox1_c, ppp_c, pwp_c);

    assertEquals(set1, set1_c);
    assertEquals(set1_c, set1);
  }
}

package ru.omsu.product.packages;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.product.products.PieceProduct;
import ru.omsu.product.products.ProductBox;
import ru.omsu.product.products.WeightProduct;

public class BatchOfProductSetTest {
  @Test
  public void getters() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);

    BatchOfProductSet batch1 = new BatchOfProductSet(new String[] { "description" }, ppp, pwp);
    BatchOfProductSet batch2 = new BatchOfProductSet(new String[] { "description" }, set1);

    assertArrayEquals(new String[] { "description" }, batch1.getDescription());
    assertArrayEquals(new Comparable[] { ppp, pwp }, batch1.getProducts());
    assertEquals(46, batch1.getMassAll());
    assertEquals(50, batch2.getMassAll());
  }

  @Test
  public void strings() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);

    BatchOfProductSet batch = new BatchOfProductSet(new String[] { "description batch" }, set1);

    assertEquals(
        "Batch of product set:\nDescription:\n\tdescription batch\nProducts:\nProduct box:\nName: set box 1, mass: 4\nPacked product set:\nProduct box:\nName: name box 1, mass: 1\nCount: 4, piece product:\nName: name, description:\n\tdescription\nMass of one: 10\nProduct box:\nName: name box 2, mass: 1\nMass: 4, weight product:\nName: name, description:\n\tdescription\nThis is weight product.",
        batch.toString());
  }

  @Test
  public void equals() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("set box 1", 4);

    ProductSet set1 = new ProductSet(setBox1, ppp, pwp);

    BatchOfProductSet batch1 = new BatchOfProductSet(new String[] { "description" }, ppp, pwp);
    BatchOfProductSet batch2 = new BatchOfProductSet(new String[] { "description" }, ppp, pwp);
    BatchOfProductSet batch3 = new BatchOfProductSet(new String[] { "description" }, set1);

    assertEquals(batch1, batch2);
    assertEquals(batch1.hashCode(), batch2.hashCode());
    assertEquals(false, batch1.equals(batch3));
    assertEquals(false, batch3.equals(batch1));
    assertEquals(false, batch1.hashCode() == batch3.hashCode());
  }
}

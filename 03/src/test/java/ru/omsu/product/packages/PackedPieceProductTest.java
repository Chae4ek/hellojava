package ru.omsu.product.packages;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.product.products.PieceProduct;
import ru.omsu.product.products.ProductBox;

public class PackedPieceProductTest {
  @Test
  public void getters() {
    ProductBox pb = new ProductBox("name box", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb, pp, 4);

    assertEquals(pb, ppp.getProductBox());
    assertEquals(pp, ppp.getProduct());
    assertEquals(4, ppp.getCount());
    assertEquals(40, ppp.getNetto());
    assertEquals(41, ppp.getBrutto());
  }

  @Test
  public void strings() {
    ProductBox pb = new ProductBox("name box", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb, pp, 4);

    assertEquals(
        "Product box:\nName: name box, mass: 1\nCount: 4, piece product:\nName: name, description:\n\tdescription\nMass of one: 10",
        ppp.toString());
  }

  @Test
  public void equals() {
    ProductBox pb = new ProductBox("name box", 1);
    ProductBox pb2 = new ProductBox("name box2", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);

    PackedPieceProduct a = new PackedPieceProduct(pb, pp, 4);
    PackedPieceProduct b = new PackedPieceProduct(pb, pp, 4);
    PackedPieceProduct c = new PackedPieceProduct(pb, pp, 5);
    PackedPieceProduct d = new PackedPieceProduct(pb2, pp, 4);

    assertEquals(a, b);
    assertEquals(b, a);
    assertEquals(a, a);
    assertEquals(a.hashCode(), b.hashCode());
    assertEquals(false, a.hashCode() == c.hashCode());
    assertEquals(false, a.equals(c));
    assertEquals(false, c.equals(a));
    assertEquals(false, a.equals(d));
    assertEquals(false, d.equals(a));
    assertEquals(false, c.equals(d));
    assertEquals(false, d.equals(c));
  }
}

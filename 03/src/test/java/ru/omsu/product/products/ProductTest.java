package ru.omsu.product.products;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProductTest {
  @Test
  public void getters() {
    class SomeProduct extends Product {
      SomeProduct(String name, String[] description) {
        super(name, description);
      }
    }

    Product p = new SomeProduct("nameS", new String[] { "desc", "ription", "" });

    assertEquals("nameS", p.getName());
    assertArrayEquals(new String[] { "desc", "ription", "" }, p.getDescription());
  }

  @Test
  public void strings() {
    class SomeProduct extends Product {
      SomeProduct(String name, String[] description) {
        super(name, description);
      }
    }

    Product p = new SomeProduct("nameS", new String[] { "desc", "ription", "" });

    assertEquals("Name: nameS, description:\n\tdesc\n\tription\n\t", p.toString());
  }

  @Test
  public void equals() {
    Product a = new WeightProduct("A", new String[] { "asd", "dsa" });
    Product d = new WeightProduct("A", new String[] { "asd", "dsa" });
    Product e = new PieceProduct("A", new String[] { "asd", "dsa" }, 10);

    assertEquals(true, a.equals(d));
    assertEquals(false, d.equals(e));

    class SomeProduct extends Product {
      SomeProduct(String name, String[] description) {
        super(name, description);
      }
    }

    Product p1 = new SomeProduct("name", new String[] { "asd", "ss" });
    Product p2 = new SomeProduct("name", new String[] { "asd", "ss" });
    Product p3 = new SomeProduct("name", new String[] { "asd", "." });

    assertEquals(p1, p2);
    assertEquals(p1.hashCode(), p2.hashCode());
    assertEquals(false, p1.equals(p3));
    assertEquals(false, p3.equals(p1));
  }
}

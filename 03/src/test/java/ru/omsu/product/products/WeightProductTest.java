package ru.omsu.product.products;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WeightProductTest {
  @Test
  public void strings() {
    WeightProduct pp = new WeightProduct("name", new String[] { "d", "a" });

    assertEquals("Name: name, description:\n\td\n\ta\nThis is weight product.", pp.toString());
  }

  @Test
  public void equals() {
    Product a1 = new WeightProduct("A", new String[] { "asd", "dsa" });
    WeightProduct b1 = new WeightProduct("A", new String[] { "asd", "dsa" });
    WeightProduct c1 = new WeightProduct("B", new String[] { "asd" });

    assertEquals(a1.hashCode(), b1.hashCode());
    assertEquals(false, a1.hashCode() == c1.hashCode());
    assertEquals(b1, b1);
    assertEquals(b1, a1);
    assertEquals(a1, b1);
    assertEquals(true, a1.equals(b1));
    assertEquals(true, b1.equals(a1));
    assertEquals(false, a1.equals(c1));
    assertEquals(false, c1.equals(a1));
    assertEquals(false, b1.equals(c1));
    assertEquals(false, c1.equals(b1));

    WeightProduct d1 = new WeightProduct("A", new String[] { "asd", "dsa" });

    assertEquals(true, b1.equals(d1));
    assertEquals(true, d1.equals(b1));
  }
}

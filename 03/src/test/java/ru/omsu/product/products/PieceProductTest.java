package ru.omsu.product.products;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PieceProductTest {
  @Test
  public void getters() {
    PieceProduct pp = new PieceProduct("name", new String[] { "d", "a" }, 12);

    assertEquals(12, pp.getMassOfOne());
    assertEquals("name", pp.getName());
    assertArrayEquals(new String[] { "d", "a" }, pp.getDescription());
  }

  @Test
  public void strings() {
    PieceProduct pp = new PieceProduct("name", new String[] { "d", "a" }, 12);

    assertEquals("Name: name, description:\n\td\n\ta\nMass of one: 12", pp.toString());
  }

  @Test
  public void equals() {
    Product a1 = new PieceProduct("A", new String[] { "asd", "dsa" }, 10);
    PieceProduct b1 = new PieceProduct("A", new String[] { "asd", "dsa" }, 10);
    PieceProduct c1 = new PieceProduct("B", new String[] { "asd" }, 10);

    assertEquals(a1.hashCode(), b1.hashCode());
    assertEquals(false, a1.hashCode() == c1.hashCode());
    assertEquals(b1, b1);
    assertEquals(b1, a1);
    assertEquals(a1, b1);
    assertEquals(true, a1.equals(b1));
    assertEquals(true, b1.equals(a1));
    assertEquals(false, a1.equals(c1));
    assertEquals(false, c1.equals(a1));
    assertEquals(false, b1.equals(c1));
    assertEquals(false, c1.equals(b1));

    PieceProduct d1 = new PieceProduct("A", new String[] { "asd", "dsa" }, 10);

    assertEquals(true, b1.equals(d1));
    assertEquals(true, d1.equals(b1));

    Product e1 = new PieceProduct("A", new String[] { "asd", "dsa" }, 11);

    assertEquals(false, d1.equals(e1));
    assertEquals(false, e1.equals(d1));
  }
}

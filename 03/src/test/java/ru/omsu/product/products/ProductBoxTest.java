package ru.omsu.product.products;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProductBoxTest {
  @Test
  public void getters() {
    ProductBox pb = new ProductBox("name", 10);

    assertEquals("name", pb.getName());
    assertEquals(10, pb.getMass());
  }

  @Test
  public void strings() {
    ProductBox a = new ProductBox("some name", 10);

    assertEquals("Name: some name, mass: 10", a.toString());
  }

  @Test
  public void equals() {
    ProductBox a = new ProductBox("name", 10);
    ProductBox b = new ProductBox("name", 10);
    ProductBox c = new ProductBox("other", 21);

    assertEquals(a, b);
    assertEquals(false, a.equals(c));
    assertEquals(false, c.equals(a));
    assertEquals(a.hashCode(), b.hashCode());
    assertEquals(false, a.hashCode() == c.hashCode());
  }
}

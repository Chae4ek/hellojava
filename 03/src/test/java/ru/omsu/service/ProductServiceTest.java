package ru.omsu.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.omsu.filter.IFilter;
import ru.omsu.product.packages.BatchOfProductSet;
import ru.omsu.product.packages.PackedPieceProduct;
import ru.omsu.product.packages.PackedWeightProduct;
import ru.omsu.product.packages.ProductSet;
import ru.omsu.product.products.PieceProduct;
import ru.omsu.product.products.ProductBox;
import ru.omsu.product.products.WeightProduct;

public class ProductServiceTest {
  @Test
  public void test() {
    ProductBox pb1 = new ProductBox("name box 1", 1);
    PieceProduct pp = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp = new PackedPieceProduct(pb1, pp, 4);

    ProductBox pb2 = new ProductBox("name box 2", 1);
    WeightProduct wp = new WeightProduct("name", new String[] { "description" });
    PackedWeightProduct pwp = new PackedWeightProduct(pb2, wp, 4);

    ProductBox setBox1 = new ProductBox("A", 4);
    ProductBox setBox2 = new ProductBox("B", 4);

    ProductBox pb3 = new ProductBox("A", 1);
    PieceProduct pp3 = new PieceProduct("name", new String[] { "description" }, 10);
    PackedPieceProduct ppp3 = new PackedPieceProduct(pb3, pp3, 4);

    ProductSet set1 = new ProductSet(setBox1, ppp3);
    ProductSet set2 = new ProductSet(setBox2, ppp3);

    BatchOfProductSet batch1 = new BatchOfProductSet(new String[] { "description" }, ppp, pwp, set1);
    BatchOfProductSet batch2 = new BatchOfProductSet(new String[] { "description" }, ppp, pwp, set2);
    BatchOfProductSet batch3 = new BatchOfProductSet(new String[] { "description" }, pwp);

    class Filter1 implements IFilter {
      @Override
      public boolean apply(String s) {
        return true;
      }
    }
    class Filter2 implements IFilter {
      @Override
      public boolean apply(String s) {
        return s.equals("A");
      }
    }
    class Filter3 implements IFilter {
      @Override
      public boolean apply(String s) {
        return false;
      }
    }

    IFilter filter1 = new Filter1();
    IFilter filter2 = new Filter2();
    IFilter filter3 = new Filter3();

    assertEquals(3, ProductService.countByFilter(batch1, filter1));
    assertEquals(1, ProductService.countByFilter(batch1, filter2));
    assertEquals(0, ProductService.countByFilter(batch2, filter2));
    assertEquals(0, ProductService.countByFilter(batch1, filter3));

    assertEquals(3, ProductService.countByFilterDeep(batch2, filter1));
    assertEquals(1, ProductService.countByFilterDeep(batch2, filter2));
    assertEquals(0, ProductService.countByFilterDeep(batch2, filter3));

    assertEquals(false, ProductService.checkAllWeighted(batch1));
    assertEquals(false, ProductService.checkAllWeighted(batch2));
    assertEquals(true, ProductService.checkAllWeighted(batch3));
  }
}

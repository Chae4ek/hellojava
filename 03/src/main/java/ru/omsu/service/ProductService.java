package ru.omsu.service;

import ru.omsu.filter.IFilter;
import ru.omsu.product.packages.BatchOfProductSet;
import ru.omsu.product.packages.IPackedProduct;
import ru.omsu.product.packages.IPackedProductSetable;
import ru.omsu.product.packages.ProductSet;
import ru.omsu.product.products.WeightProduct;

public abstract class ProductService {
  /**
   * Возвращает количество элементов партии, имена которых удовлетворяют фильтру
   */
  public static int countByFilter(BatchOfProductSet batch, IFilter filter) {
    int count = 0;

    for (IPackedProductSetable product : batch.getProducts()) {
      if (filter.apply(product.getProductBox().getName())) {
        ++count;
      }
    }

    return count;
  }

  /**
   * Ведет себя аналогично методу countByFilter, но набор считается подходящим,
   * если он содержит (возможно на некоторой глубине) хотя бы один товар с
   * названием, удовлетворяющим фильтру
   */
  public static int countByFilterDeep(BatchOfProductSet batch, IFilter filter) {
    int count = 0;

    for (IPackedProductSetable product : batch.getProducts()) {
      if (product.getClass() == ProductSet.class) {
        if (containsByFilterDeep((ProductSet) product, filter)) {
          ++count;
        }
      } else if (filter.apply(product.getProductBox().getName())) {
        ++count;
      }
    }

    return count;
  }

  private static boolean containsByFilterDeep(ProductSet product, IFilter filter) {
    for (IPackedProductSetable p : product.getProducts()) {
      if (p.getClass() == ProductSet.class) {
        return containsByFilterDeep((ProductSet) p, filter);
      } else {
        return filter.apply(p.getProductBox().getName());
      }
    }
    return false;
  }

  /**
   * Проверяет, что вся партия состоит только из весовых товаров
   */
  public static boolean checkAllWeighted(BatchOfProductSet batch) {
    for (IPackedProductSetable product : batch.getProducts()) {
      if (product.getClass() == ProductSet.class) {
        return false;
      } else if (((IPackedProduct) product).getProduct().getClass() != WeightProduct.class) {
        return false;
      }
    }
    return true;
  }
}

package ru.omsu.filter;

public class ContainsFilter implements IFilter {
  private String pattern;

  public ContainsFilter(String pattern) {
    this.pattern = pattern;
  }

  /**
   * Проверяет, что s содержит pattern
   */
  @Override
  public boolean apply(String s) {
    return s.contains(pattern);
  }

}

package ru.omsu.filter;

public class BeginStringFilter implements IFilter {
  private String pattern;

  public BeginStringFilter(String pattern) {
    this.pattern = pattern;
  }

  /**
   * Проверяет, что строка s начинается с подстроки pattern
   */
  @Override
  public boolean apply(String s) {
    if (s.length() < pattern.length()) {
      return false;
    }

    for (int i = 0; i < pattern.length(); ++i) {
      if (pattern.charAt(i) != s.charAt(i)) {
        return false;
      }
    }

    return true;
  }

}

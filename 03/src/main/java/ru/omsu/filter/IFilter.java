package ru.omsu.filter;

public interface IFilter {
  boolean apply(String s);
}

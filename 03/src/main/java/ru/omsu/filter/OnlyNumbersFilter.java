package ru.omsu.filter;

public class OnlyNumbersFilter implements IFilter {
  public OnlyNumbersFilter() {
  }

  /**
   * Проверяет, что s состоит лишь и только лишь из цифр
   */
  @Override
  public boolean apply(String s) {
    if (s == null || s.length() == 0) {
      return false;
    }

    for (char c : s.toCharArray()) {
      if (c < '0' || c > '9') {
        return false;
      }
    }

    return true;
  }
}

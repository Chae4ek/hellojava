package ru.omsu.product.packages;

public class BatchOfProductSet extends Comparable {
  String[] description;
  IPackedProductSetable[] products;

  public BatchOfProductSet(String[] description, IPackedProductSetable... products) {
    if (description == null || products == null) {
      throw new NullPointerException("bad description or products");
    }
    for (String s : description) {
      if (s == null) {
        throw new NullPointerException("bad description");
      }
    }
    for (IPackedProductSetable pps : products) {
      if (pps == null) {
        throw new NullPointerException("bad products");
      }
    }

    this.description = description;
    this.products = products;
  }

  public int getMassAll() {
    int mass = 0;
    for (IPackedProductSetable ps : products) {
      mass += ps.getBrutto();
    }
    return mass;
  }

  public String[] getDescription() {
    return description;
  }

  public IPackedProductSetable[] getProducts() {
    return products;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    BatchOfProductSet temp = (BatchOfProductSet) obj;

    if (description.length == temp.description.length && products.length == temp.products.length) {
      for (int i = 0; i < description.length; ++i) {
        if (!description[i].equals(temp.description[i])) {
          return false;
        }
      }
      for (int i = 0; i < products.length; ++i) {
        if (!products[i].equals(temp.products[i])) {
          return false;
        }
      }
      return true;
    }

    return false;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    for (String s : description) {
      result = prime * result + s.hashCode();
    }
    for (IPackedProductSetable ps : products) {
      result = prime * result + ps.hashCode();
    }
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Batch of product set:\nDescription:");
    for (String s : description) {
      sb.append("\n\t").append(s);
    }
    sb.append("\nProducts:");
    for (IPackedProductSetable ps : products) {
      sb.append('\n').append(ps.toString());
    }
    return sb.toString();
  }
}

package ru.omsu.product.packages;

import ru.omsu.product.products.Product;
import ru.omsu.product.products.ProductBox;
import ru.omsu.product.products.WeightProduct;

public class PackedWeightProduct extends Comparable implements IPackedProduct, IPackedProductSetable {
  private ProductBox productBox;
  private WeightProduct weightProduct;
  private int mass;

  public PackedWeightProduct(ProductBox productBox, WeightProduct weightProduct, int mass) {
    if (productBox == null || weightProduct == null) {
      throw new NullPointerException("bad productBox or weightProduct");
    }

    this.productBox = productBox;
    this.weightProduct = weightProduct;
    this.mass = mass;
  }

  @Override
  public ProductBox getProductBox() {
    return productBox;
  }

  @Override
  public Product getProduct() {
    return weightProduct;
  }

  public int getMass() {
    return mass;
  }

  @Override
  public int getNetto() {
    return mass;
  }

  @Override
  public int getBrutto() {
    return getNetto() + productBox.getMass();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    PackedWeightProduct temp = (PackedWeightProduct) obj;

    return mass == temp.mass && productBox.equals(temp.productBox) && weightProduct.equals(temp.weightProduct);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + productBox.hashCode();
    result = prime * result + weightProduct.hashCode();
    result = prime * result + mass;
    return result;
  }

  @Override
  public String toString() {
    return String.format("Product box:\n%s\nMass: %d, weight product:\n%s", productBox.toString(), mass,
        weightProduct.toString());
  }
}

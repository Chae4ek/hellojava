package ru.omsu.product.packages;

import ru.omsu.product.products.Product;

public interface IPackedProduct {
  Product getProduct();
}

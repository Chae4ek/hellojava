package ru.omsu.product.packages;

public abstract class Comparable {
  @Override
  public abstract boolean equals(Object obj);

  @Override
  public abstract int hashCode();

  @Override
  public abstract String toString();
}

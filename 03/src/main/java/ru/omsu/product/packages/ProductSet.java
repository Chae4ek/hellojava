package ru.omsu.product.packages;

import ru.omsu.product.products.ProductBox;

public class ProductSet extends Comparable implements IPackedProductSetable {
  ProductBox productBox;
  IPackedProductSetable[] products;

  public ProductSet(ProductBox productBox, IPackedProductSetable... products) {
    if (productBox == null || products == null) {
      throw new NullPointerException("bad productBox or products");
    }
    for (IPackedProductSetable product : products) {
      if (product == null) {
        throw new NullPointerException("bad products");
      }
    }

    this.productBox = productBox;
    this.products = products;
  }

  @Override
  public ProductBox getProductBox() {
    return productBox;
  }

  public IPackedProductSetable[] getProducts() {
    return products;
  }

  @Override
  public int getNetto() {
    int mass = 0;
    for (IPackedProductSetable product : products) {
      mass += product.getBrutto();
    }
    return mass;
  }

  @Override
  public int getBrutto() {
    return getNetto() + productBox.getMass();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    ProductSet temp = (ProductSet) obj;

    if (products.length == temp.products.length && productBox.equals(temp.productBox)) {
      for (int i = 0; i < products.length; ++i) {
        if (!products[i].equals(temp.products[i])) {
          return false;
        }
      }
      return true;
    }

    return false;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + productBox.hashCode();
    if (products == null) {
      result *= prime;
    } else {
      for (IPackedProductSetable product : products) {
        result = prime * result + product.hashCode();
      }
    }
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("Product box:\n%s\nPacked product set:", productBox.toString()));
    for (IPackedProductSetable product : products) {
      sb.append('\n').append(product.toString());
    }
    return sb.toString();
  }
}

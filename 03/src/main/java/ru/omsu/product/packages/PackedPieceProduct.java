package ru.omsu.product.packages;

import ru.omsu.product.products.PieceProduct;
import ru.omsu.product.products.Product;
import ru.omsu.product.products.ProductBox;

public class PackedPieceProduct extends Comparable implements IPackedProduct, IPackedProductSetable {
  private ProductBox productBox;
  private PieceProduct pieceProduct;
  private int count;

  public PackedPieceProduct(ProductBox productBox, PieceProduct pieceProduct, int count) {
    if (productBox == null || pieceProduct == null) {
      throw new NullPointerException("bad productBox or pieceProduct");
    }

    this.productBox = productBox;
    this.pieceProduct = pieceProduct;
    this.count = count;
  }

  @Override
  public ProductBox getProductBox() {
    return productBox;
  }

  @Override
  public Product getProduct() {
    return pieceProduct;
  }

  public int getCount() {
    return count;
  }

  @Override
  public int getNetto() {
    return pieceProduct.getMassOfOne() * count;
  }

  @Override
  public int getBrutto() {
    return getNetto() + productBox.getMass();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    PackedPieceProduct temp = (PackedPieceProduct) obj;

    return count == temp.count && productBox.equals(temp.productBox) && pieceProduct.equals(temp.pieceProduct);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + productBox.hashCode();
    result = prime * result + pieceProduct.hashCode();
    result = prime * result + count;
    return result;
  }

  @Override
  public String toString() {
    return String.format("Product box:\n%s\nCount: %d, piece product:\n%s", productBox.toString(), count,
        pieceProduct.toString());
  }
}

package ru.omsu.product.packages;

import ru.omsu.product.products.ProductBox;

public interface IPackedProductSetable {
  ProductBox getProductBox();

  int getNetto();

  int getBrutto();
}

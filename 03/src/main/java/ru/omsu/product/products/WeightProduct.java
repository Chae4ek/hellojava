package ru.omsu.product.products;

public class WeightProduct extends Product {
  public WeightProduct(String name, String[] description) {
    super(name, description);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    return super.equals(obj);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode() * prime;
    return result;
  }

  @Override
  public String toString() {
    return super.toString() + "\nThis is weight product.";
  }
}

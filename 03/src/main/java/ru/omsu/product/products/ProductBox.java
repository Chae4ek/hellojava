package ru.omsu.product.products;

public class ProductBox {
  private String name;
  private int mass;

  public ProductBox(String name, int mass) {
    if (name == null) {
      throw new NullPointerException("bad name");
    }

    this.name = name;
    this.mass = mass;
  }

  public String getName() {
    return name;
  }

  public int getMass() {
    return mass;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    ProductBox temp = (ProductBox) obj;

    return mass == temp.mass && name.equals(temp.name);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + name.hashCode();
    result = prime * result + mass;
    return result;
  }

  @Override
  public String toString() {
    return String.format("Name: %s, mass: %d", name, mass);
  }
}

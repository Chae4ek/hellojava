package ru.omsu.product.products;

public class PieceProduct extends Product {
  private int massOfOne;

  public PieceProduct(String name, String[] description, int massOfOne) {
    super(name, description);
    this.massOfOne = massOfOne;
  }

  public int getMassOfOne() {
    return massOfOne;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    PieceProduct temp = (PieceProduct) obj;

    return massOfOne == temp.massOfOne && super.equals(obj);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + massOfOne;
    return result;
  }

  @Override
  public String toString() {
    return super.toString() + String.format("\nMass of one: %d", massOfOne);
  }
}

package ru.omsu.product.products;

public abstract class Product {
  private String name;
  private String[] description;

  public Product(String name, String[] description) {
    if (name == null || description == null) {
      throw new NullPointerException("bad name or description");
    }
    for (String s : description) {
      if (s == null) {
        throw new NullPointerException("bad description");
      }
    }

    this.name = name;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public String[] getDescription() {
    return description;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    Product temp = (Product) obj;

    if (description.length == temp.description.length && name.equals(temp.name)) {
      for (int i = 0; i < description.length; ++i) {
        if (!description[i].equals(temp.description[i])) {
          return false;
        }
      }
      return true;
    }

    return false;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + name.hashCode();
    if (description == null) {
      result *= prime;
    } else {
      for (String s : description) {
        result = prime * result + s.hashCode();
      }
    }
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("Name: %s, description:", name));
    for (String s : description) {
      sb.append("\n\t").append(s);
    }
    return sb.toString();
  }
}

package ru.omsu;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import ru.omsu.demo.Cat;
import ru.omsu.demo.Human;
import ru.omsu.demo.Man;

public class TestReflectionDemo {

  @Test
  public void test() {
    List<Object> objects = new ArrayList<>();
    objects.add(new Human());
    objects.add(new Man());
    objects.add(new Cat());

    Assert.assertEquals(2, ReflectionDemo.getHumanOrSubclassCount(objects));

    List<String> s = ReflectionDemo.getListPublicMethods(new Human());
    final List<String> ans = new ArrayList<>();
    ans.add("nameHumanMethod");
    ans.add("wait");
    ans.add("wait");
    ans.add("wait");
    ans.add("equals");
    ans.add("toString");
    ans.add("hashCode");
    ans.add("getClass");
    ans.add("notify");
    ans.add("notifyAll");

    Assert.assertEquals(ans, s);

    ans.clear();

    Assert.assertThrows(NullPointerException.class, () -> ReflectionDemo.getSuperClasses(null));

    Class<?> clazz = Human.class;
    ans.add("java.lang.Object");

    Assert.assertEquals(ans, ReflectionDemo.getSuperClasses(clazz));

    Man man = new Man();
    ans.clear();
    ans.add("ru.omsu.demo.Human");
    ans.add("java.lang.Object");

    Assert.assertEquals(ans, ReflectionDemo.getSuperClasses(man));

    objects.clear();
    objects.add(new Cat());
    objects.add(new Man());
    objects.add(new Human());
    objects.add(new Man());
    objects.add(null);
    ReflectionDemo.executeAll(objects);
    Assert.assertEquals(2, Man.test);

    ans.clear();
    ans.add("setT");
    ans.add("getT");
    Assert.assertEquals(ans, ReflectionDemo.getGettersAndSetters(new Cat()));
  }
}

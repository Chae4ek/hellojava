package ru.omsu;

@FunctionalInterface
public interface Executable {

  void execute();
}

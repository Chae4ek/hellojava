package ru.omsu.demo;

import ru.omsu.Executable;

public class Man extends Human implements Executable {

  public static int test;

  @Override
  public void execute() {
    ++test;
  }
}

package ru.omsu;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import ru.omsu.demo.Human;

public class ReflectionDemo {

  public static <T> int getHumanOrSubclassCount(List<T> list) {
    int count = 0;
    for (var i : list) {
      if (i instanceof Human) {
        ++count;
      }
    }
    return count;
  }

  public static List<String> getListPublicMethods(Object obj) {
    Method[] m = obj.getClass().getMethods();
    List<String> s = new ArrayList<>();
    for (var i : m) {
      s.add(i.getName());
    }
    return s;
  }

  public static List<String> getSuperClasses(Object obj) {
    List<String> s = new ArrayList<>();
    Class<?> curr = obj.getClass();
    do {
      curr = curr.getSuperclass();
      s.add(curr.getName());
    } while (curr != Object.class);
    return s;
  }

  /**
   * @return count of the Executable elements
   */
  public static <T> int executeAll(List<T> list) {
    int count = 0;
    for (var i : list) {
      if (i instanceof Executable) {
        ((Executable) i).execute();
        ++count;
      }
    }
    return count;
  }

  public static List<String> getGettersAndSetters(Object obj) {
    List<String> s = new ArrayList<>();
    for (var i : obj.getClass().getDeclaredMethods()) {
      if ((i.getModifiers() & Modifier.PUBLIC) != 0 && (i.getModifiers() & Modifier.STATIC) == 0) {
        if (i.getName().startsWith("get") && i.getParameterCount() == 0 && i.getReturnType() != void.class) {
          s.add(i.getName());
        } else if (i.getName().startsWith("set") && i.getParameterCount() == 1 && i.getReturnType() == void.class) {
          s.add(i.getName());
        }
      }
    }
    return s;
  }
}

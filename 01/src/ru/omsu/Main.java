package ru.omsu;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) throws Exception {
    // prog1();
    // prog2_3();
    // prog4(1, 0, -1);
    // prog5(); // fixed!
    // prog6();
    // prog7(); // fixed!
    // prog8_14(); // fixed!
    // prog15();

    prog16();
    prog17();
    prog18();
  }

  public static void prog1() {
    System.out.println("Hello, World!");
  }

  public static void prog2_3() {
    Scanner input = new Scanner(System.in);

    double a = input.nextDouble();
    double b = input.nextDouble();
    double c = input.nextDouble();

    input.close();

    double max = Math.max(Math.max(a, b), c);
    double min = Math.min(Math.min(a, b), c);
    double mid = a - max + b - min + c;

    System.out.println("Multiple: " + (a * b * c));
    System.out.println("Average: " + (a + b + c) / 3.);
    System.out.println("To upper: " + min + " " + mid + " " + max);
  }

  private static void prog4(double a, double b, double c) {
    final double discriminant = b * b - 4 * a * c;

    if (a == 0) {
      if (b == 0) {
        if (c == 0) {
          System.out.println("There are any roots");
        } else
          System.out.println("There are no roots");
      } else {
        System.out.println("Root: " + (c / b));
      }
    } else if (discriminant < 0) {
      System.out.println("There are no real roots");
    } else if (discriminant == 0) {
      if (-b / 2 / a != 0)
        System.out.println("Root: " + (-b / 2 / a));
      else
        System.out.println("Root: " + 0.);// fix output -0.0
    } else {
      System.out.println(
          "Roots: " + ((-b + Math.sqrt(discriminant)) / 2 / a) + " and " + ((-b - Math.sqrt(discriminant)) / 2 / a));
    }
  }

  public static void prog5() {
    System.out.println("Input A, B and C for tabulating sin(x) of [A, B] with C step: ");

    Scanner input = new Scanner(System.in);

    double a = input.nextDouble();
    double b = input.nextDouble();
    double c = input.nextDouble();

    input.close();

    for (; a < b || Math.abs(a - b) < 1e-13; a += c) {
      System.out.println("x: " + a + " sin(x): " + Math.sin(a));
    }
  }

  public static void prog6() {
    System.out.println("{ Ax + By = C\n{ Dx + Ey = F");
    System.out.println("Input A, B, C, D, E and F: ");

    Scanner input = new Scanner(System.in);

    double a = input.nextDouble();
    double b = input.nextDouble();
    double c = input.nextDouble();
    double d = input.nextDouble();
    double e = input.nextDouble();
    double f = input.nextDouble();

    input.close();

    double D = a * e - b * d;
    double D1 = c * e - b * f;
    double D2 = a * f - d * c;

    double x = D1 / D;
    double y = D2 / D;

    if (D == 0) {
      System.out.println("There are no roots or infinitely many");
    } else {
      System.out.println("x: " + x + " y: " + y);
    }
  }

  public static void prog7() {
    Scanner input = new Scanner(System.in);

    System.out.println("Input x: ");
    double x = input.nextDouble();

    System.out.println("Input precision for exp(x): ");
    double eps = input.nextDouble();

    input.close();

    double sum = 1;
    double n = 1;
    double prev = 1;
    while (Math.abs(prev) >= eps) {
      prev *= x / n;
      sum += prev;
      ++n;
    }
    System.out.println("Answer: " + sum);
  }

  public static void prog8_14() {
    System.out.println("input array:");
    Scanner in = new Scanner(System.in);
    String str = in.nextLine();
    String[] strs = str.split(" ");
    in.close();
    int[] intArray = new int[strs.length];
    int i = 0;
    for (String s : strs) {
      intArray[i++] = Integer.parseInt(s);
    }
    Array ar = new Array(intArray);

    ar.printArray();
    System.out.println("sum of all elements: " + ar.sumOfAllElements());
    System.out.println("count of even numbers: " + ar.countOfEvenNumbers());
    final int a = 0, b = 3;
    System.out.println("count of numbers in segment [" + a + ", " + b + "]: " + ar.countOfNumbersInSegment(a, b));
    System.out.println("is all numbers positive: " + ar.areAllNumbersPositive());

    ar.reverseArray();
    System.out.println("reverse array: ");
    ar.printArray();
  }

  public static void prog15() {
    Point3D p1 = new Point3D(3, 2, 1);
    Point3D p2 = new Point3D(1, 2, 3);

    System.out.println(p1.equals(p2));
    System.out.println(p1.equals(p1));
    System.out.println(p1 == p2);
  }

  public static void prog16() {
    Point3D p1 = new Point3D();
    Point3D p2 = new Point3D(1, 2, 3);

    Vector3D vec1 = new Vector3D(1, 2, 3);
    Vector3D vec2 = new Vector3D(p1, p2);

    System.out.println("vec1.length: " + vec1.length() + "\nvec2.length: " + vec2.length());
    System.out.println(vec1.equals(vec2));
  }

  public static void prog17() {
    Vector3D v1 = new Vector3D(1, 1, 1);
    Vector3D v2 = new Vector3D(2, 2, 2);

    Vector3D res = Vector3DProcessor.sumOf(v1, v2);
    System.out.println("x: " + res.x + " y: " + res.y + " z: " + res.z);

    res = Vector3DProcessor.subtractOf(v1, v2);
    System.out.println("x: " + res.x + " y: " + res.y + " z: " + res.z);

    System.out.println("dot: " + Vector3DProcessor.dotProductOf(v1, v2));

    res = Vector3DProcessor.vectorProductOf(v1, v2);
    System.out.println("x: " + res.x + " y: " + res.y + " z: " + res.z);

    System.out.println("are collinear: " + Vector3DProcessor.areCollinear(v1, v2));
  }

  public static void prog18() throws Exception {
    Vector3DArray ar = new Vector3DArray(5);

    System.out.println(ar.countVectors());

    Vector3D v = new Vector3D(1, 2, 3);
    ar.setVector(2, v);
    System.out.println(ar.getVectorAt(2).x + " " + ar.getVectorAt(2).y + " " + ar.getVectorAt(2).z);

    System.out.println("max length of vectors: " + ar.findMaxLengthOfVector());

    System.out.println("index of first equaled vector: " + ar.getIndexOfFirstEqualTo(v));
    System.out.println(
        "index of first equaled vector (expected not found): " + ar.getIndexOfFirstEqualTo(new Vector3D(0, -1, 2)));

    v = ar.sumOfAllVectors();
    System.out.println("sum of all vectors: " + v.x + " " + v.y + " " + v.z);

    v = ar.linearCombination(1, 1, 1, 1, 1);
    System.out.println("linear combination: " + v.x + " " + v.y + " " + v.z);

    Point3D[] points = ar.movePointToVectorsFrom(new Point3D(3, 2, 1));
    System.out.println("points:");
    for (Point3D p : points) {
      System.out.println(p.getX() + " " + p.getY() + " " + p.getZ());
    }
  }
}
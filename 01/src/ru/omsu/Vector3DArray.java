package ru.omsu;

public class Vector3DArray {
  private Vector3D[] vectors;

  public Vector3DArray(final int count) {
    vectors = new Vector3D[count];

    for (int i = 0; i < count; ++i) {
      vectors[i] = new Vector3D();
    }
  }

  public int countVectors() {
    return vectors.length;
  }

  public void setVector(final int index, final Vector3D vector) {
    if (index >= 0 && index < vectors.length) {
      vectors[index] = vector;
    }
  }

  public Vector3D getVectorAt(int index) {
    if (index < vectors.length && index >= 0) {
      return vectors[index];
    }
    return null;
  }

  public double findMaxLengthOfVector() {
    double maxLength = 0;

    for (Vector3D v : vectors) {
      double temp = v.length();
      if (temp > maxLength) {
        maxLength = temp;
      }
    }

    return maxLength;
  }

  public int getIndexOfFirstEqualTo(final Vector3D vector) {
    for (int i = 0; i < vectors.length; ++i) {
      if (vectors[i].equals(vector)) {
        return i;
      }
    }
    return -1;
  }

  public Vector3D sumOfAllVectors() {
    Vector3D vector = new Vector3D();

    for (Vector3D v : vectors) {
      vector = Vector3DProcessor.sumOf(vector, v);
    }

    return vector;
  }

  public Vector3D linearCombination(final double... quotients) throws Exception {
    if (quotients.length != vectors.length) {
      throw new Exception("count quotients is not equal to count of vectors");
    }

    Vector3D vector = new Vector3D();

    for (int i = 0; i < vectors.length; ++i) {
      vector = Vector3DProcessor.sumOf(vector,
          new Vector3D(vectors[i].x * quotients[i], vectors[i].y * quotients[i], vectors[i].z * quotients[i]));
    }

    return vector;
  }

  public Point3D[] movePointToVectorsFrom(final Point3D point) {
    Point3D[] points = new Point3D[vectors.length];

    for (int i = 0; i < vectors.length; ++i) {
      points[i] = new Point3D(point.getX() + vectors[i].x, point.getY() + vectors[i].y, point.getZ() + vectors[i].z);
    }

    return points;
  }
}
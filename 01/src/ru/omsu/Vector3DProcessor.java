package ru.omsu;

public abstract class Vector3DProcessor {
  public static Vector3D sumOf(final Vector3D v1, final Vector3D v2) {
    return new Vector3D(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
  }

  public static Vector3D subtractOf(final Vector3D minuend, final Vector3D subtrahend) {
    return new Vector3D(minuend.x - subtrahend.x, minuend.y - subtrahend.y, minuend.z - subtrahend.z);
  }

  public static double dotProductOf(final Vector3D v1, final Vector3D v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
  }

  public static Vector3D vectorProductOf(final Vector3D v1, final Vector3D v2) {
    return new Vector3D(v1.y * v2.z - v2.y * v1.z, v1.x * v2.z - v2.x * v1.x, v1.x * v2.y - v2.x * v1.y);
  }

  public static boolean areCollinear(final Vector3D v1, final Vector3D v2) {
    if (Math.abs(v1.y * v2.z - v2.y * v1.z) < 1e-13 && Math.abs(v1.x * v2.z - v2.x * v1.x) < 1e-13
        && Math.abs(v1.x * v2.y - v2.x * v1.y) < 1e-13) {
      return true;
    }
    return false;
  }
}
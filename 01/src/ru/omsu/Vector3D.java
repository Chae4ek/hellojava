package ru.omsu;

public class Vector3D {
  public double x;
  public double y;
  public double z;

  public Vector3D() {
  }

  public Vector3D(final double x, final double y, final double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public Vector3D(final Point3D startPoint, final Point3D endPoint) {
    this.x = endPoint.getX() - startPoint.getX();
    this.y = endPoint.getY() - startPoint.getY();
    this.z = endPoint.getZ() - startPoint.getZ();
  }

  public double length() {
    return Math.sqrt(x * x + y * y + z * z);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    Vector3D v = (Vector3D) obj;

    return Math.abs(v.x - this.x) < 1e-13 && Math.abs(v.y - this.y) < 1e-13 && Math.abs(v.z - this.z) < 1e-13;
  }
}
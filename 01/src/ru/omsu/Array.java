package ru.omsu;

public class Array {
  private int[] array;

  public Array(final int... array) {
    this.array = array;
  }

  public void printArray() {
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.print("\n");
  }

  public long sumOfAllElements() {
    long sum = 0;

    for (int i : array) {
      sum += i;
    }

    return sum;
  }

  public int countOfEvenNumbers() {
    int count = 0;

    for (int i : array) {
      if (i % 2 == 0) {
        ++count;
      }
    }

    return count;
  }

  public int countOfNumbersInSegment(int a, int b) {
    if (a > b) {
      int temp = a;
      a = b;
      b = temp;
    }

    int count = 0;

    for (int i : array) {
      if (i >= a && i <= b) {
        ++count;
      }
    }

    return count;
  }

  public boolean areAllNumbersPositive() {
    for (int i : array) {
      if (i <= 0) {
        return false;
      }
    }

    return true;
  }

  public void reverseArray() {
    for (int i = 0, j = array.length - 1; i < j; ++i, --j) {
      int temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }
}
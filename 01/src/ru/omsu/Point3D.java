package ru.omsu;

public class Point3D {
  private double x;
  private double y;
  private double z;

  public Point3D() {
  }

  public Point3D(final double x, final double y, final double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public double getZ() {
    return z;
  }

  public void setX(final double x) {
    this.x = x;
  }

  public void setY(final double y) {
    this.y = y;
  }

  public void setZ(final double z) {
    this.z = z;
  }

  public void printCoords() {
    System.out.print("x: " + x + " y: " + y + " z: " + z);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    Point3D point = (Point3D) obj;

    return Math.abs(point.x - this.x) < 1e-13 && Math.abs(point.y - this.y) < 1e-13
        && Math.abs(point.z - this.z) < 1e-13;
  }
}
package ru.omsu;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

public class StreamHandler {

  public static void writeIntArray(OutputStream out, int[] array) throws IOException {
    try (var o = new DataOutputStream(new BufferedOutputStream(out))) {
      for (int i : array) {
        o.writeInt(i);
      }
    }
  }

  public static void readIntArray(InputStream in, int[] array) throws IOException {
    try (var input = new DataInputStream(in)) {
      for (int i = 0; i < array.length; ++i) {
        array[i] = input.readInt();
      }
    }
  }

  public static void writeIntArray(Writer out, int[] array) throws IOException {
    out = new BufferedWriter(out);
    for (int i : array) {
      out.write(Integer.toString(i));
      out.write(' ');
    }
    out.flush();
  }

  public static void readIntArray(Reader in, int[] array) throws IOException {
    StringBuilder sb = new StringBuilder();
    int p = 0;
    while (p != array.length) {
      int c = in.read();
      if (c != ' ') {
        sb.append(Integer.parseInt(Character.toString(c)));
      }
      if (c == ' ') {
        if (!sb.toString().equals("")) {
          array[p++] = Integer.parseInt(sb.toString());
          sb.setLength(0);
        }
      }
    }
  }

  public static void readIntArray(RandomAccessFile in, int[] array, int start) throws IOException {
    in.seek(start * 4);
    for (int i = 0; i < array.length; ++i) {
      array[i] = in.readInt();
    }
  }

  public static List<String> lsWithExtension(File directory, String extension) throws IOException {
    return ls(directory, ".*[.]" + extension, false, false, false);
  }

  public static List<String> lsWithSubdirectory(File directory, String regExName) throws IOException {
    return ls(directory, regExName, true, true, true);
  }

  public static List<String> ls(File directory, String regExNameFilter, boolean getDirectoryNames, boolean findInSubdirectories, boolean getAbsolutePath) {
    List<String> list = new LinkedList<>();
    ls(list, directory, regExNameFilter, getDirectoryNames, findInSubdirectories, getAbsolutePath);
    return list;
  }

  private static void ls(List<String> list, File directory, String regExNameFilter, boolean getDirectoryNames, boolean findInSubdirectories, boolean getAbsolutePath) {
    File[] files = directory.listFiles();
    if (files == null) {
      return;
    }

    for (File f : files) {
      if (getDirectoryNames || f.isFile()) {
        if (f.getName().matches(regExNameFilter)) {
          if (getAbsolutePath) {
            list.add(f.getAbsolutePath());
          } else {
            list.add(f.getName());
          }
        }
      }

      if (findInSubdirectories && f.isDirectory()) {
        ls(list, f, regExNameFilter, getDirectoryNames, findInSubdirectories, getAbsolutePath);
      }
    }
  }
}

package ru.omsu.property;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public abstract class HouseService {

  public static void serializeJavaStandard(House h, ObjectOutputStream into) throws IOException {
    into.writeObject(h);
  }

  public static House deserializeJavaStandard(ObjectInputStream into) throws IOException, ClassNotFoundException {
    return (House) into.readObject();
  }

  public static String serializeJackson(House h) throws JsonProcessingException {
    return new ObjectMapper().writeValueAsString(h);
  }

  public static House deserializeJackson(String json) throws JsonProcessingException {
    return new ObjectMapper().readValue(json, House.class);
  }
}

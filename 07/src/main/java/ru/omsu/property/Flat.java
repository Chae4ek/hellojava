package ru.omsu.property;

import java.io.Serializable;
import java.util.List;

public class Flat implements Serializable {

  private int number;
  private double area;
  private List<Person> owners;

  public Flat() {
  }

  public Flat(int number, double area, List<Person> owners) {
    this.number = number;
    this.area = area;
    this.owners = owners;
  }

  public int getNumber() {
    return number;
  }

  public double getArea() {
    return area;
  }

  public List<Person> getOwners() {
    return owners;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    Flat o = (Flat) obj;
    return number == o.number && Math.abs(area - o.area) < 1e-13 && owners.equals(o.owners);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + number;
    result = prime * result + Double.hashCode(area * 1000);
    result = prime * result + owners.hashCode();
    return result;
  }
}

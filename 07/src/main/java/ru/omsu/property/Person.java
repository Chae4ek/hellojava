package ru.omsu.property;

import java.io.Serializable;

public class Person implements Serializable {

  protected int asd;
  private String firstName;
  private String secondName;
  private String middleName;
  private String birthdayDate;

  public Person() {
  }

  public Person(String firstName, String secondName, String middleName, String birthdayDate) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.middleName = middleName;
    this.birthdayDate = birthdayDate;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getBirthdayDate() {
    return birthdayDate;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    Person o = (Person) obj;
    return firstName.equals(o.firstName) && secondName.equals(o.secondName) && middleName.equals(o.middleName)
            && birthdayDate.equals(o.birthdayDate);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + firstName.hashCode();
    result = prime * result + secondName.hashCode();
    result = prime * result + middleName.hashCode();
    result = prime * result + birthdayDate.hashCode();
    return result;
  }
}

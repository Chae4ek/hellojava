package ru.omsu.property;

import java.io.Serializable;
import java.util.List;

public class House implements Serializable {

  private String cadastralNumber;
  private String address;
  private Person housekeeper;
  private List<Flat> flats;

  public House() {
  }

  public House(String cadastralNumber, String address, Person housekeeper, List<Flat> flats) {
    this.cadastralNumber = cadastralNumber;
    this.address = address;
    this.housekeeper = housekeeper;
    this.flats = flats;
  }

  public String getCadastralNumber() {
    return cadastralNumber;
  }

  public String getAddress() {
    return address;
  }

  public Person getHousekeeper() {
    return housekeeper;
  }

  public List<Flat> getFlats() {
    return flats;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    House o = (House) obj;
    return cadastralNumber.equals(o.cadastralNumber) && address.contains(o.address) && housekeeper.equals(o.housekeeper)
            && flats.equals(o.flats);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + cadastralNumber.hashCode();
    result = prime * result + address.hashCode();
    result = prime * result + housekeeper.hashCode();
    result = prime * result + flats.hashCode();
    return result;
  }
}

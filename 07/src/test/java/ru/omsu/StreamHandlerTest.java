package ru.omsu;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamHandlerTest {

  @Test
  public void testIntArrayStreams() throws IOException {
    int[] ar1 = new int[]{1, 2, 3, 4, 5, 6};
    int[] ar2 = new int[]{1};
    int[] ar3 = new int[]{};

    try (var out = new ByteArrayOutputStream()) {
      StreamHandler.writeIntArray(out, ar1);
      try (var in = new ByteArrayInputStream(out.toByteArray())) {
        int[] ans = new int[6];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar1, ans);
      }
    }

    try (var out = new ByteArrayOutputStream()) {
      StreamHandler.writeIntArray(out, ar1);
      try (var in = new ByteArrayInputStream(out.toByteArray())) {
        int[] ans = new int[2];
        StreamHandler.readIntArray(in, ans);
        for (int i = 0; i < ans.length; ++i) {
          assertEquals(ar1[i], ans[i]);
        }
        int[] ans2 = new int[4];
        StreamHandler.readIntArray(in, ans2);
        for (int i = 0; i < ans.length; ++i) {
          assertEquals(ar1[i + 2], ans2[i]);
        }
      }
    }

    try (var out = new ByteArrayOutputStream()) {
      StreamHandler.writeIntArray(out, ar2);
      try (var in = new ByteArrayInputStream(out.toByteArray())) {
        int[] ans = new int[1];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar2, ans);
      }
    }

    try (var out = new ByteArrayOutputStream()) {
      StreamHandler.writeIntArray(out, ar3);
      try (var in = new ByteArrayInputStream(out.toByteArray())) {
        int[] ans = new int[0];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar3, ans);
      }
    }
  }

  @Test
  public void testIntArrayReaderAndWriter() throws IOException {
    int[] ar1 = new int[]{1, 1999, 3, 4, 5, 6};
    int[] ar2 = new int[]{1};
    int[] ar3 = new int[]{};

    try (var out = new CharArrayWriter()) {
      StreamHandler.writeIntArray(out, ar1);
      try (var in = new CharArrayReader(out.toCharArray())) {
        int[] ans = new int[6];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar1, ans);
      }
    }

    try (var out = new CharArrayWriter()) {
      StreamHandler.writeIntArray(out, ar1);
      try (var in = new CharArrayReader(out.toCharArray())) {
        int[] ans = new int[2];
        StreamHandler.readIntArray(in, ans);
        for (int i = 0; i < ans.length; ++i) {
          assertEquals(ar1[i], ans[i]);
        }
        int[] ans2 = new int[4];
        StreamHandler.readIntArray(in, ans2);
        for (int i = 0; i < ans.length; ++i) {
          assertEquals(ar1[i + 2], ans2[i]);
        }
      }
    }

    try (var out = new CharArrayWriter()) {
      StreamHandler.writeIntArray(out, ar2);
      try (var in = new CharArrayReader(out.toCharArray())) {
        int[] ans = new int[1];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar2, ans);
      }
    }

    try (var out = new CharArrayWriter()) {
      StreamHandler.writeIntArray(out, ar3);
      try (var in = new CharArrayReader(out.toCharArray())) {
        int[] ans = new int[0];
        StreamHandler.readIntArray(in, ans);
        assertArrayEquals(ar3, ans);
      }
    }
  }

  @Test
  public void testRandomAccessFile() throws FileNotFoundException, IOException {
    var file = new File("testRandomAccessFile");
    try (var f = new RandomAccessFile(file, "rw")) {
      int[] ints = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
      for (int i = 0; i < 10; i++) {
        f.writeInt(ints[i]);
      }
      int[] ans = new int[3];
      StreamHandler.readIntArray(f, ans, 1);
      assertArrayEquals(new int[]{2, 3, 4}, ans);
    }
    file.delete();
  }

  @Test
  public void testLS() throws IOException {
    /**
     * Required methods for each file/dir:
     * - listFiles
     * - isFile
     * - isDirectory
     * - getAbsolutePath
     * - getPath
     * - getName
     */
    var file3 = mock(File.class);
    when(file3.listFiles()).thenReturn(null);
    when(file3.isFile()).thenReturn(true);
    when(file3.isDirectory()).thenReturn(false);
    when(file3.getAbsolutePath()).thenReturn("/absolute/path/dirRoot/file3.exe");
    when(file3.getPath()).thenReturn("dirRoot/file3.exe");
    when(file3.getName()).thenReturn("file3.exe");
    var file2 = mock(File.class);
    when(file2.listFiles()).thenReturn(null);
    when(file2.isFile()).thenReturn(true);
    when(file2.isDirectory()).thenReturn(false);
    when(file2.getAbsolutePath()).thenReturn("/absolute/path/dirRoot/file2.txt");
    when(file2.getPath()).thenReturn("dirRoot/file2.txt");
    when(file2.getName()).thenReturn("file2.txt");
    var dir2 = mock(File.class);
    when(dir2.listFiles()).thenReturn(new File[]{});
    when(dir2.isFile()).thenReturn(false);
    when(dir2.isDirectory()).thenReturn(true);
    when(dir2.getAbsolutePath()).thenReturn("/absolute/path/dirRoot/dir2");
    when(dir2.getPath()).thenReturn("dirRoot/dir2");
    when(dir2.getName()).thenReturn("dir2");
    var file1 = mock(File.class);
    when(file1.listFiles()).thenReturn(null);
    when(file1.isFile()).thenReturn(true);
    when(file1.isDirectory()).thenReturn(false);
    when(file1.getAbsolutePath()).thenReturn("/absolute/path/dirRoot/dir1/file1.txt");
    when(file1.getPath()).thenReturn("dirRoot/dir1/file1.txt");
    when(file1.getName()).thenReturn("file1.txt");
    var dir1 = mock(File.class);
    when(dir1.listFiles()).thenReturn(new File[]{file1});
    when(dir1.isFile()).thenReturn(false);
    when(dir1.isDirectory()).thenReturn(true);
    when(dir1.getAbsolutePath()).thenReturn("/absolute/path/dirRoot/dir1");
    when(dir1.getPath()).thenReturn("dirRoot/dir1");
    when(dir1.getName()).thenReturn("dir1");
    var dirRoot = mock(File.class);
    when(dirRoot.listFiles()).thenReturn(new File[]{dir1, dir2, file2, file3});
    /**
     * Tree:
     * - dirRoot
     * | - dir1
     * | | - file1.txt
     * | - dir2
     * | - file2.txt
     * | - file3.exe
     */

//    var ans = new FileWriter("test");
//    for (var i : StreamHandler.lsWithSubdirectory(dirRoot, ".*")) {
//      ans.write(i);
//      ans.write('\n');
//    }
//    ans.flush();
//    ans.close();
    List<String> ans1 = new ArrayList<>();
    ans1.add("/absolute/path/dirRoot/dir1");
    ans1.add("/absolute/path/dirRoot/dir1/file1.txt");
    ans1.add("/absolute/path/dirRoot/dir2");
    ans1.add("/absolute/path/dirRoot/file2.txt");
    ans1.add("/absolute/path/dirRoot/file3.exe");
    assertEquals(ans1, StreamHandler.lsWithSubdirectory(dirRoot, ".*"));

    List<String> ans2 = new ArrayList<>();
    ans2.add("file2.txt");
    assertEquals(ans2, StreamHandler.lsWithExtension(dirRoot, "txt"));
  }
}

package ru.omsu.property;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class HouseServiceTest {

  @Test
  public void test() throws IOException, ClassNotFoundException {
    List<Person> owners = new ArrayList<>();
    owners.add(new Person("first name", "second name", "middle name", "1/1/1"));
    owners.add(new Person("_xXx_nagibator-3000+500_xXx_", "2", "like name", "21/21/1"));
    List<Flat> flats = new ArrayList<>();
    flats.add(new Flat(1, 32, owners));
    House h = new House("123-cadastral-number", "st. xxx", new Person("owner", "2nd na", "mid name", "1/1/1"), flats);

    var info = new ByteArrayOutputStream();
    try (var out = new ObjectOutputStream(info)) {
      HouseService.serializeJavaStandard(h, out);
    }
    try (var in = new ObjectInputStream(new ByteArrayInputStream(info.toByteArray()))) {
      House ans = HouseService.deserializeJavaStandard(in);
      assertEquals(h, ans);
    }

    String json = HouseService.serializeJackson(h);
    House ans = HouseService.deserializeJackson(json);
    assertEquals(h, ans);
  }
}
